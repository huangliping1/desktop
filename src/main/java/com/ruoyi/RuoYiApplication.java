package com.ruoyi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

import com.kunghsu.kunwebuploaderdemo.servlet.FileUploadServlet;
import com.kunghsu.kunwebuploaderdemo.servlet.UploadActionServlet;


/**
 * 启动程序
 * 
 * @author ruoyi
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class RuoYiApplication
{
    public static void main(String[] args)
    {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(RuoYiApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  若依启动成功  ");
    }
    
	//定义servlet，注册
    
   /*
	@Bean
	public ServletRegistrati onBean fileUploadServlet(){
		//指定访问的url
		return new ServletRegistrationBean(new FileUploadServlet(),"/FileUploadServlet");
	}

	@Bean
	public ServletRegistrationBean uploadActionServlet(){
		return new ServletRegistrationBean(new UploadActionServlet(),"/UploadActionServlet");
	}
	*/
  	
}