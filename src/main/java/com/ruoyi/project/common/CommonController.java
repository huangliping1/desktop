package com.ruoyi.project.common;

import java.awt.Frame;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;

import com.ruoyi.common.utils.CacheUtil;
import com.ruoyi.common.utils.DateUtils;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.ruoyi.common.utils.Return;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.common.utils.security.ShiroUtils;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.framework.config.RuoYiConfig;
import com.ruoyi.framework.config.ServerConfig;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.monitor.online.domain.UserOnline;
import com.ruoyi.project.monitor.online.service.UserOnlineServiceImpl;
import com.ruoyi.project.mooc.domain.MoocDirectlive;
import com.ruoyi.project.mooc.domain.MoocSerialNumber;
import com.ruoyi.project.mooc.domain.MoocUpdatefile;
import com.ruoyi.project.mooc.domain.MoocVideo;
import com.ruoyi.project.mooc.domain.MoocVideop;
import com.ruoyi.project.mooc.service.IMoocSerialNumberService;
import com.ruoyi.project.mooc.service.MoocDirectliveServiceImpl;
import com.ruoyi.project.mooc.service.MoocSerialNumberServiceImpl;
import com.ruoyi.project.mooc.service.MoocUpdatefileServiceImpl;
import com.ruoyi.project.mooc.service.MoocVideoServiceImpl;
import com.ruoyi.project.system.user.domain.User;
import com.ruoyi.project.system.user.service.UserServiceImpl;

import it.sauronsoftware.jave.Encoder;

import it.sauronsoftware.jave.MultimediaInfo;

import org.apache.tomcat.util.http.fileupload.disk.DiskFileItem;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import com.ruoyi.project.mooc.domain.Course;
import com.ruoyi.project.mooc.domain.Page;
import com.ruoyi.project.mooc.domain.Blog;




/**
 * 通用请求处理
 * 
 * @author ruoyi
 */
@Controller
public class CommonController extends  BaseController  // implements  Exception
{
	 private String ueditor = "/ueditor";
	 private String prefix = "mooc/fileupdown";
	 private String rtmpplay = "mooc/rtmpplay";
	 private String prefixvideo_show = "mooc/index";
	 
    private static final Logger log = LoggerFactory.getLogger(CommonController.class);

    @Autowired
    private ServerConfig serverConfig;

    


    /**
     * 通用下载请求
     * 
     * @param fileName 文件名称
     * @param delete 是否删除
     */
    @GetMapping("common/download")
    public void fileDownload(String fileName, Boolean delete, HttpServletResponse response, HttpServletRequest request)
    {
        try
        {
            if (!FileUtils.isValidFilename(fileName))
            {
                throw new Exception(StringUtils.format("文件名称({})非法，不允许下载。 ", fileName));
            }
            String realFileName = System.currentTimeMillis() + fileName.substring(fileName.indexOf("_") + 1);
            String filePath = RuoYiConfig.getDownloadPath() + fileName;

            response.setCharacterEncoding("utf-8");
            response.setContentType("multipart/form-data");
            response.setHeader("Content-Disposition",
                    "attachment;fileName=" + FileUtils.setFileDownloadHeader(request, realFileName));
            FileUtils.writeBytes(filePath, response.getOutputStream());
            if (delete)
            {
                FileUtils.deleteFile(filePath);
            }
        }
        catch (Exception e)
        {
            log.error("下载文件失败", e);
        }
    }

    
    @GetMapping("common/down")
    public void fileDown(String fileName, Boolean delete, HttpServletResponse response, HttpServletRequest request)
    {
        try
        {
            if (!FileUtils.isValidFilename(fileName))
            {
                throw new Exception(StringUtils.format("文件名称({})非法，不允许下载。 ", fileName));
            }
            String realFileName = System.currentTimeMillis() + fileName.substring(fileName.indexOf("_") + 1);
            String filePath = RuoYiConfig.getUploadPath() +"//"+ fileName;

            response.setCharacterEncoding("utf-8");
            response.setContentType("multipart/form-data");
            response.setHeader("Content-Disposition",
                    "attachment;fileName=" + FileUtils.setFileDownloadHeader(request, realFileName));
            FileUtils.writeBytes(filePath, response.getOutputStream());
            if (delete)
            {
              //  FileUtils.deleteFile(filePath);
            }
        }
        catch (Exception e)
        {
            log.error("下载文件失败", e);
        }
    }
    /**
     * 通用上传请求
     */
    @PostMapping("/common/upload")
    @ResponseBody
    public AjaxResult uploadFile(MultipartFile file) throws Exception
    {
        try
        {
            // 上传文件路径
            String filePath = RuoYiConfig.getUploadPath();
            // 上传并返回新文件名称
            String fileName = FileUploadUtils.upload(filePath, file);
            String url = serverConfig.getUrl() + fileName;
            AjaxResult ajax = AjaxResult.success();
            ajax.put("fileName", fileName);
            ajax.put("url", url);
            return ajax;
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }
    }
    
 
    
    @GetMapping("common/video_show")
    public String video_show()
    {
    	  return prefixvideo_show + "/index";
    }
    
    @GetMapping("common/videodetil")
    public String videodetil()
    {
    	  return prefixvideo_show + "/chapter.html";
    }
    @GetMapping("common/blogdetil")
    public String blogdetil()
    {
    	  return prefixvideo_show + "/post.html";
    }
    
    
    
    @GetMapping("common/getHomeCourseList")
    @ResponseBody
    public Return getHomeCourseList() {
    	List<List> courseList = new ArrayList<>(2);
    	Course  sc = new Course();
    	sc.setId(0);
    	sc.setKindName("语法");
    	sc.setIfLike(10);
    	sc.setLike(10);
    	sc.setStar(2);
    	sc.setIfStar(true);
    	sc.setUserHeadUrl("http://127.0.0.1/profile/upload/img/2020/eb4ef3803510ea29236c3d8a3951bc68.jpg");
    	sc.setCourseHeadUrl("http://127.0.0.1/profile/upload/img/2020/2992e7245587a4b77e99b7da6fdfef8e.jpg");
    	sc.setUserId(0);
    	sc.setIntroduction("C 语言是一种通用的、面向过程式的计算机程序设计语言。1972 年，为了移植与开发 UNIX 操作系统，丹尼斯·里奇在贝尔电话实验室设计开发了 C 语言。\r\n" + 
    			"\r\n" + 
    			"C 语言是一种广泛使用的计算机语言，它与 Java 编程语言一样普及，二者在现代软件程序员之间都得到广泛使用。");
    	sc.setTitle("C入门教学");
    	sc.setUserNickName("张三");
    	
    	
    	  MoocVideoServiceImpl moocVideoService = SpringUtils.getBean(MoocVideoServiceImpl.class);
    	  MoocVideo moocVideo=new MoocVideo();
    	  List<MoocVideo> list = moocVideoService.selectMoocVideoList(moocVideo);
    	  MoocVideop moocVideop=new MoocVideop();
    	  List<MoocVideop> listp = moocVideoService.selectMoocVideoListp(moocVideop);
    	  
    //	  UserServiceImpl IUserService= SpringUtils.getBean(UserServiceImpl.class);
    //	  User  user=new User();
   // 	  user.setUserName(userName);
   //      String loginName = user.;
          
    	Map pmap =new HashMap();
    	Map hmap =new HashMap();
    	
    	
   // 	{"id":34,"userId":2,"kindName":"GO","title":"asdf","introduction":"ertg","createdTime":1586761923000,"like":0,"star":0,"courseHeadUrl":"M00/00/00/wKhjZF6UEMOAEnZpAAA_U-mwqEc677.jpg","userNickName":"东云1号","userHeadUrl":"M00/00/00/wKhjZF6UIHyAQH5QAAAyFQcnWvM855.jpg","ifStar":false,"ifLike":false,"chapterList":null,"commentList":null},{"id":33,"userId":2,"kindName":"GO","title":"asdf","introduction":"ertg","createdTime":1586761896000,"like":0,"star":0,"courseHeadUrl":"M00/00/00/wKhjZF6UEKiAdOFJAAA_U-mwqEc385.jpg","userNickName":"东云1号","userHeadUrl":"M00/00/00/wKhjZF6UIHyAQH5QAAAyFQcnWvM855.jpg","ifStar":false,"ifLike":false,"chapterList":null,"commentList":null},
    	
    	 hmap.put("age",11);
         hmap.put("id","33");
         hmap.put("sex","man");
         hmap.put("name","fei");
         hmap.put("addr","neyok");

    	pmap=entityToMap(sc);
    	//pmap.putAll(sc.toString());
    	List<Map<String,Object>> coursest=new ArrayList<Map<String,Object>>();
    	coursest.add(pmap);
    	coursest.add(pmap);
    	coursest.add(pmap);
    	coursest.add(pmap);
    	courseList.add(listp);
    	courseList.add(coursest);
        return new Return(courseList);
        
       //[[{commentList=null, star=0, chapterList=null, like=0, title=null, userId=0, userHeadUrl=null, courseHeadUrl=null, kindName=null, createdTime=null, ifLike=true, id=0, userNickName=null, introduction=null, ifStar=true}
    }
    
    
    
    @GetMapping("common/getblogList")
    @ResponseBody
    public Return getblogList() {
    	List<List> blogList = new ArrayList<>(2);
    	Blog ps = new Blog();
    	ps.setLike(10);
    	ps.setStar(2);
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
    	Date createdTime = new Date();
    	String time =df.format(new Date());
		ps.setCreatedTime(createdTime);
    	ps.setUserId(0);
    	ps.setTitle("C入门教学");
    	ps.setContent("C 语言是一种通用的、面向过程式的计算机程序设计语言。1972 年，为了移植与开发 UNIX 操作系统，丹尼斯·里奇在贝尔电话实验室设计开发了 C 语言。\r\n" + 
    			"\r\n" + 
    			"C 语言是一种广泛使用的计算机语言，它与 Java 编程语言一样普及，二者在现代软件程序员之间都得到广泛使用。");
    	ps.setNickName("张三");
    	ps.setHeadimg("http://127.0.0.1/profile/upload/img/2020/eb4ef3803510ea29236c3d8a3951bc68.jpg");
    	Date ifLike=new Date();
		ps.setIfLike(ifLike);
    	Date ifStar =new Date();
		ps.setIfStar(ifStar);
    	ps.setKindName(null);
    	ps.setCreatedTime(null);
    	Map pmap =new HashMap();
    	pmap=entityToMap(ps);
    	List<Map<String,Object>> blog=new ArrayList<Map<String,Object>>();
    	blog.add(pmap);
    	blog.add(pmap);
    	blogList.add(blog);
    	blogList.add(blog);
        return new Return(blogList);
    }
    
    
    
    
    @PostMapping("common/getCourse")
    @ResponseBody
    public Return getCourse(HttpServletRequest request, long courseId) {
    	
    	  if (courseId <= 0) return Return.CLIENT_PARAM_ERROR;
       //   long userId = CacheUtil.getUserId(request);
          
    	List<List> courseList = new ArrayList<>(2);
    
    	MoocVideoServiceImpl moocVideoService = SpringUtils.getBean(MoocVideoServiceImpl.class);
    	MoocVideop moocVideop=new MoocVideop();
    	moocVideop.setVideoId(courseId);
    	List<MoocVideop> listp = moocVideoService.selectMoocVideoListp(moocVideop);
    	List<MoocVideop> listchapter =null;
    	if(listp.size()>0&&listp.get(0).getVideoName().length()>0)
    	{
    	Map hmap=new HashMap();
    	hmap=entityToMap(listp.get(0));
        listp.get(0).getVideoId();
        moocVideop=new MoocVideop();
    	moocVideop.setVideoName(listp.get(0).getVideoName());
    	 listchapter = moocVideoService.selectMoocVideoListp(moocVideop);
    	}
    	
    	Map pmap =new HashMap();
    	Map hmap =new HashMap();
    	
    	List<Map<String,Object>> coursest=new ArrayList<Map<String,Object>>();
    	coursest.add(pmap);
    	coursest.add(pmap);
    	coursest.add(pmap);
    	coursest.add(pmap);
    	
    	courseList.add(listp);
    	courseList.add(listchapter);
        return new Return(courseList);
        
     }
    
    
    @GetMapping("common/downindex")
    public String downindex()
    {
    	  return prefix + "/picup";
    }
    
    @GetMapping("common/downindexlive")
    public String downindexlive()
    {
    	  return prefix + "/picuplive";
    }
    @GetMapping("common/videoupindex")
    public String videoupindex()
    {
    	  return prefix + "/videoup";
    }
    
    @GetMapping("common/videoplay")
    public String videoplay()
    {
    	  return prefix + "/videoup";
    }
    
    @GetMapping("common/rtmpplay")
    public String rtmpplay()
    {
    	  return rtmpplay + "/index.html";
    }
    
    @GetMapping("common/h5play")
    public String h5play()
    {
    	  return rtmpplay + "/h5.html";
    }
    
    @GetMapping("common/ueditor")
    public String ueditor()
    {
    	  return ueditor + "/index.html";
    }
    
    
    
    public static Map<String, Object> entityToMap(Object object) {
		Map<String, Object> map = new HashMap();
	    for (Field field : object.getClass().getDeclaredFields()){
	        try {
	        	boolean flag = field.isAccessible();
	            field.setAccessible(true);
	            Object o = field.get(object);
	            map.put(field.getName(), o);
	            field.setAccessible(flag);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	    return map;
	}
	
	/**
	 * Map转实体类
	 * @param map 需要初始化的数据，key字段必须与实体类的成员名字一样，否则赋值为空
	 * @param entity  需要转化成的实体类
	 * @return
	 */
	public static <T> T mapToEntity(Map<String, Object> map, Class<T> entity) {
		T t = null;
		try {
			t = entity.newInstance();
			for(Field field : entity.getDeclaredFields()) {
				if (map.containsKey(field.getName())) {
					boolean flag = field.isAccessible();
		            field.setAccessible(true);
		            Object object = map.get(field.getName());
		            if (object!= null && field.getType().isAssignableFrom(object.getClass())) {
		            	 field.set(t, object);
					}
		            field.setAccessible(flag);
				}
			}
			return t;
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return t;
	}


    /**
     * 图片上传请求
     */
    @PostMapping("/common/uploadpic")
    @ResponseBody
    public AjaxResult uploadFilepic(MultipartFile file,String id) throws Exception
    {
        try
        {
            // 上传文件路径
            String filePath = RuoYiConfig.getUploadPath();
            // 上传并返回新文件名称
            String name=file.getOriginalFilename();
            String type=name.substring(name.lastIndexOf(".") + 1);
            long size=file.getSize();
            BigDecimal b1 = new BigDecimal(size);
            BigDecimal mb = new BigDecimal("1024");
            BigDecimal b2 = b1.divide(mb).divide(mb);
            
            String fileName = FileUploadUtils.uploadpic(filePath, file);

    //        int timeout = 10;
    //        Date expiredDate = DateUtils.addMilliseconds(new Date(), 0 - timeout);
    //        UserOnlineServiceImpl userOnlineService = SpringUtils.getBean(UserOnlineServiceImpl.class);
   //         List<UserOnline> userOnlineList = userOnlineService.selectOnlineByExpired(expiredDate);
            
            MoocVideoServiceImpl moocVideoService = SpringUtils.getBean(MoocVideoServiceImpl.class);
            MoocVideo moocVideo = moocVideoService.selectMoocVideoById(Long.parseLong(id));
           // moocVideo.setVideoId(Long.parseLong(id));
            String url = serverConfig.getUrl() + fileName;
            moocVideo.setVideoPic(url);
            moocVideoService.updateMoocVideo(moocVideo);

            
   /* */	Map pmap=new HashMap();
    		pmap.put("seralname", "fileseral");
    	    String ss=CommonController.getseralnumber(pmap);
    	    
    	    MoocUpdatefileServiceImpl  moocUpdatefileService = SpringUtils.getBean(MoocUpdatefileServiceImpl.class);
    	    MoocUpdatefile moocUpdatefile =new MoocUpdatefile();//=  moocUpdatefileService.selectMoocUpdatefileById(ss);
    	    moocUpdatefile.setIdName(ss);
    	    moocUpdatefile.setFileFerequency("0");
    	    moocUpdatefile.setFileOrdertype("图片");
    	    moocUpdatefile.setFileFormat(type);
    	    moocUpdatefile.setFileLength(b2.toString().substring(0, 15)+"M");
    	    moocUpdatefile.setFileOriginalname(name);
    	    moocUpdatefile.setRemark(fileName);
    	    moocUpdatefile.setIdDesc(url);
    	    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");//注意月份是MM
    	    moocUpdatefile.setLastDate(DateUtils.getNowDate().toString());
    	    moocUpdatefileService.insertMoocUpdatefile(moocUpdatefile);
    	    String operName = ShiroUtils.getLoginName();
    	    moocUpdatefile.setCreaterBy(operName);
    	   
            AjaxResult ajax = AjaxResult.success();
            ajax.put("fileName", fileName);
            ajax.put("url", url);
            return ajax;
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }
    }
    
    
    
    /**
     * 图片上传请求
     */
    @PostMapping("/common/uploadpiclive")
    @ResponseBody
    public AjaxResult uploadFilepiclive(MultipartFile file,String id) throws Exception
    {
        try
        {
            // 上传文件路径
            String filePath = RuoYiConfig.getUploadPath();
            // 上传并返回新文件名称
            String name=file.getOriginalFilename();
            String type=name.substring(name.lastIndexOf(".") + 1);
            long size=file.getSize();
            BigDecimal b1 = new BigDecimal(size);
            BigDecimal mb = new BigDecimal("1024");
            BigDecimal b2 = b1.divide(mb).divide(mb);
            
            String fileName = FileUploadUtils.uploadpic(filePath, file);
    
            MoocDirectliveServiceImpl moocDirectliveService = SpringUtils.getBean(MoocDirectliveServiceImpl.class);
            MoocDirectlive moocDirectlive = moocDirectliveService.selectMoocDirectliveById(Long.parseLong(id));
            
          //  MoocVideo moocVideo = moocVideoService.selectMoocVideoById(Long.parseLong(id));
            String url = serverConfig.getUrl() + fileName;
            moocDirectlive.setVideoPic(url);
            moocDirectliveService.updateMoocDirectlive(moocDirectlive);
            
            
            Map pmap=new HashMap();
    		pmap.put("seralname", "fileseral");
    	    String ss=CommonController.getseralnumber(pmap);
    	    
    	    MoocUpdatefileServiceImpl  moocUpdatefileService = SpringUtils.getBean(MoocUpdatefileServiceImpl.class);
    	    MoocUpdatefile moocUpdatefile =new MoocUpdatefile();
    	    moocUpdatefile.setIdName(ss);
    	    moocUpdatefile.setFileFerequency("0");
    	    moocUpdatefile.setFileOrdertype("图片");
    	    moocUpdatefile.setFileFormat(type);
    	    moocUpdatefile.setFileLength(b2.toString().substring(0, 15)+"M");
    	    moocUpdatefile.setFileOriginalname(name);
    	    moocUpdatefile.setRemark(fileName);
    	    moocUpdatefile.setIdDesc(url);
    	    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");//注意月份是MM
    	    moocUpdatefile.setLastDate(DateUtils.getNowDate().toString());
    	    moocUpdatefileService.insertMoocUpdatefile(moocUpdatefile);
    	    String operName = ShiroUtils.getLoginName();
    	    moocUpdatefile.setCreaterBy(operName);
    	    
    	    
            AjaxResult ajax = AjaxResult.success();
            ajax.put("fileName", fileName);
            ajax.put("url", url);
            return ajax;
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }
    }
    /**
     * 视频上传请求
     */
    @PostMapping("/common/uploadvideo")
    @ResponseBody
    public AjaxResult uploadFilevideo(MultipartFile file,String id) throws Exception
    {
        try
        {
            // 上传文件路径
            String filePath = RuoYiConfig.getUploadPath();
            // 上传并返回新文件名称
            String name=file.getOriginalFilename();
            String type=name.substring(name.lastIndexOf(".") + 1);   
            long size=file.getSize();
            BigDecimal b1 = new BigDecimal(size);
            BigDecimal mb = new BigDecimal("1024");
            BigDecimal b2 = b1.divide(mb).divide(mb);
            String time=ReadVideoTimeMs(file);
            time= ReadVideoTime(Long.parseLong(time));
            String fileName = FileUploadUtils.uploadvideo(filePath, file);
            MoocVideoServiceImpl moocVideoService = SpringUtils.getBean(MoocVideoServiceImpl.class);
            MoocVideo moocVideo = moocVideoService.selectMoocVideoById(Long.parseLong(id));
            String url = serverConfig.getUrl() + fileName;
            String videoLoad=url.substring(url.lastIndexOf("upload") + 0);
            moocVideo.setVideoPlaytime(time);
            moocVideo.setVideoAddress(url);
            moocVideo.setVideoLoad(videoLoad);
            moocVideoService.updateMoocVideo(moocVideo);
            
            
            Map pmap=new HashMap();
    		pmap.put("seralname", "fileseral");
    	    String ss=CommonController.getseralnumber(pmap);
    	    
    	    MoocUpdatefileServiceImpl  moocUpdatefileService = SpringUtils.getBean(MoocUpdatefileServiceImpl.class);
    	    MoocUpdatefile moocUpdatefile =new MoocUpdatefile();
    	    moocUpdatefile.setIdName(ss);
    	    moocUpdatefile.setFileFerequency("0");
    	    moocUpdatefile.setFileOrdertype("视频");
    	    moocUpdatefile.setFileFormat(type);
    	    moocUpdatefile.setFileLength(b2.toString().substring(0, 15)+"M");
    	    moocUpdatefile.setFileOriginalname(name);
    	    moocUpdatefile.setRemark(fileName);
    	    moocUpdatefile.setIdDesc(url);
    	    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");//注意月份是MM
    	    moocUpdatefile.setLastDate(DateUtils.getNowDate().toString());
    	    moocUpdatefileService.insertMoocUpdatefile(moocUpdatefile);
    	    String operName = ShiroUtils.getLoginName();
    	    moocUpdatefile.setCreaterBy(operName);
    	    
            AjaxResult ajax = AjaxResult.success();
            ajax.put("fileName", fileName);
            ajax.put("url", url);
            return ajax;
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }
    }   
    /*
    https://blog.csdn.net/weixin_45637293/article/details/101704639
    public static String getTempPath(String tempPath, File file2, Map map) {

        long currentTimeMillis = System.currentTimeMillis();

        String[] split = tempPath.split("/");
        String path="";

        for (String s : split) {
            if (!s.equals(split[split.length-1])){
                path+=s+"/";
            }
        }
        path+=currentTimeMillis+".jpg";

        File targetFile = new File(path);
        if (!targetFile.getParentFile().exists()) {
            targetFile.getParentFile().mkdirs();
        }
        try {
            if (file2.exists()) {
                FFmpegFrameGrabber ff = new FFmpegFrameGrabber(file2);
                ff.start();
                int ftp = ff.getLengthInFrames();
                int flag = 0;
                Frame frame = null;
                while (flag <= ftp) {
                    //获取帧
                    frame = ff.grabImage();
                    //过滤前3帧，避免出现全黑图片
                    if ((flag > 3) && (frame != null)) {
                        break;
                    }
                    flag++;
                }

                ImageIO.write(FrameToBufferedImage(frame), "jpg", targetFile);

                ff.close();
                ff.stop();

                System.out.println(path);

                map.put("snapshoot",path);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static RenderedImage FrameToBufferedImage(Frame frame) {
        //创建BufferedImage对象
        Java2DFrameConverter converter = new Java2DFrameConverter();
        BufferedImage bufferedImage = converter.getBufferedImage(frame);
        return bufferedImage;
    }

	*/
    /**
     * 获取视频时长(时分秒)
     *
     * @param ms
     * @return
     */
    public static String ReadVideoTime(long ms) {
        int ss = 1000;
        int mi = ss * 60;
        int hh = mi * 60;
        int dd = hh * 24;

        long day = ms / dd;
        long hour = (ms - day * dd) / hh;
        long minute = (ms - day * dd - hour * hh) / mi;
        long second = (ms - day * dd - hour * hh - minute * mi) / ss;
        long milliSecond = ms - day * dd - hour * hh - minute * mi - second * ss;

        String strDay = day < 10 ? "0" + day : "" + day; //天
        String strHour = hour < 10 ? "0" + hour : "" + hour;//小时
        String strMinute = minute < 10 ? "0" + minute : "" + minute;//分钟
        String strSecond = second < 10 ? "0" + second : "" + second;//秒
        String strMilliSecond = milliSecond < 10 ? "0" + milliSecond : "" + milliSecond;//毫秒
        strMilliSecond = milliSecond < 100 ? "0" + strMilliSecond : "" + strMilliSecond;
        if (strHour.equals("00")) {
            return strMinute + ":" + strSecond;
        } else {
            return strHour + ":" + strMinute + ":" + strSecond;
        }
    }

    /**
     * 获取视频时长(毫秒)
     *
     * @param file
     * @return
     */
    public static String ReadVideoTimeMs(MultipartFile file) {
        Encoder encoder = new Encoder();
        String length = "";
        try {
            // 获取文件类型
            String fileName = file.getContentType();
            // 获取文件后缀
            String pref = fileName.indexOf("/") != -1 ? fileName.substring(fileName.lastIndexOf("/") + 1, fileName.length()) : null;
            String prefix = "." + pref;
            // 用uuid作为文件名，防止生成的临时文件重复
            final File excelFile = File.createTempFile(UUID.randomUUID().toString(), prefix);
            // MultipartFile to File
            file.transferTo(excelFile);
            MultimediaInfo m = encoder.getInfo(excelFile);
            long ls = m.getDuration();
            length = String.valueOf(ls);
            //程序结束时，删除临时文件
            deleteFile(excelFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return length;
    }

    /**
     * 删除
     *
     * @param files
     */
    private static void deleteFile(File... files) {
        for (File file : files) {
            if (file.exists()) {
                file.delete();
            }
        }
    }
    
    
    
    /**
     * 获取流水并更新(时分秒)
     *
     *
     *
     *  SimpleDateFormat myFmt=new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
        SimpleDateFormat myFmt1=new SimpleDateFormat("yy/MM/dd HH:mm");
        SimpleDateFormat myFmt2=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//等价于now.toLocaleString()
        SimpleDateFormat myFmt3=new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒 E ");
        SimpleDateFormat myFmt4=new SimpleDateFormat(
                "一年中的第 D 天 一年中第w个星期 一月中第W个星期 在一天中k时 z时区");
     * @param ms
     * @return
     */
    public static String getseralnumber(Map pamp ) {
      
        String seralnumber = ""; 
        String seralname ="videoseralnumber"; //
      //  seralnumber=pamp.get("seralnumber").toString();
        seralname=pamp.get("seralname").toString();
         
       MoocSerialNumberServiceImpl IMoocSerialNumberService = SpringUtils.getBean(MoocSerialNumberServiceImpl.class);
        
       //   moocSerialNumber = new MoocSerialNumber();
       MoocSerialNumber moocSerialNumber = IMoocSerialNumberService.selectMoocSerialNumberById(seralname.toString());


       String  timeformat=moocSerialNumber.getDateFormat();
       Date now = new Date(); // 创建一个Date对象，获取当前时间
       // 指定格式化格式
       SimpleDateFormat f = new SimpleDateFormat(timeformat);
       String time =f.format(now);
       
       String CurSeqId= moocSerialNumber.getCurSeqId().toString();
       String numbername=moocSerialNumber.getRuleStr();
       
       long number=Long.parseLong(CurSeqId)+1;
       int seqlenth=moocSerialNumber.getSeqLength();
       String sizes="%0"+seqlenth+"d";
    //   CurSeqId=time+numbername+String.format("%06d",number);
       CurSeqId=numbername+time+String.format(sizes,number);
       moocSerialNumber.setCurSeqId(number);
       moocSerialNumber.setIdDesc(CurSeqId);
       
       
       IMoocSerialNumberService.updateMoocSerialNumber(moocSerialNumber);
       return CurSeqId;
       }
  }

