package com.ruoyi.project.mooc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 mooc_answer
 * 
 * @author administrator
 * @date 2020-05-06
 */
public class MoocAnswer extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 答案编号 */
    private String answerId;

    /** 问题答案关联号 */
    @Excel(name = "问题答案关联号")
    private String answerRelated;

    /** 回答类型 */
    @Excel(name = "回答类型")
    private String answerType;

    /** 回答大类 */
    @Excel(name = "回答大类")
    private String answerMaintype;

    /** 答案 */
    @Excel(name = "答案")
    private String answerDescribe;

    /** 回答人 */
    @Excel(name = "回答人")
    private String answerPeople;

    /** 回答时间 */
    @Excel(name = "回答时间")
    private String answerTime;

    /** 问题编号 */
    @Excel(name = "问题编号")
    private String answerNumber;

    /** 回答老师 */
    @Excel(name = "回答老师")
    private String answerTeacher;

    /** 相似问题 */
    @Excel(name = "相似问题")
    private String answerLiker;

    /** 答案采纳率 */
    @Excel(name = "答案采纳率")
    private String answerAcception;

    /** 最迟回答时间 */
    @Excel(name = "最迟回答时间")
    private String answerLatetime;

    /** 回答处理状态 */
    @Excel(name = "回答处理状态")
    private String answerState;

    /** 回答评论 */
    @Excel(name = "回答评论")
    private String answerComment;

    /** 回答排序 */
    @Excel(name = "回答排序")
    private Integer answerSort;

    /** 问题关联课程 */
    @Excel(name = "问题关联课程")
    private String answerCourse;

    /** 最近使用日期 */
    @Excel(name = "最近使用日期")
    private String lastDate;

    /** 状态 */
    @Excel(name = "状态")
    private String stateId;

    /** 创建者 */
    @Excel(name = "创建者")
    private String createrBy;

    /** 更新人 */
    @Excel(name = "更新人")
    private String updaterName;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private String updaterTime;

    /** 版本号 */
    @Excel(name = "版本号")
    private String versionNumber;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    /** 公司机构名 */
    @Excel(name = "公司机构名")
    private String conpanyCodee;

    public void setAnswerId(String answerId) 
    {
        this.answerId = answerId;
    }

    public String getAnswerId() 
    {
        return answerId;
    }
    public void setAnswerRelated(String answerRelated) 
    {
        this.answerRelated = answerRelated;
    }

    public String getAnswerRelated() 
    {
        return answerRelated;
    }
    public void setAnswerType(String answerType) 
    {
        this.answerType = answerType;
    }

    public String getAnswerType() 
    {
        return answerType;
    }
    public void setAnswerMaintype(String answerMaintype) 
    {
        this.answerMaintype = answerMaintype;
    }

    public String getAnswerMaintype() 
    {
        return answerMaintype;
    }
    public void setAnswerDescribe(String answerDescribe) 
    {
        this.answerDescribe = answerDescribe;
    }

    public String getAnswerDescribe() 
    {
        return answerDescribe;
    }
    public void setAnswerPeople(String answerPeople) 
    {
        this.answerPeople = answerPeople;
    }

    public String getAnswerPeople() 
    {
        return answerPeople;
    }
    public void setAnswerTime(String answerTime) 
    {
        this.answerTime = answerTime;
    }

    public String getAnswerTime() 
    {
        return answerTime;
    }
    public void setAnswerNumber(String answerNumber) 
    {
        this.answerNumber = answerNumber;
    }

    public String getAnswerNumber() 
    {
        return answerNumber;
    }
    public void setAnswerTeacher(String answerTeacher) 
    {
        this.answerTeacher = answerTeacher;
    }

    public String getAnswerTeacher() 
    {
        return answerTeacher;
    }
    public void setAnswerLiker(String answerLiker) 
    {
        this.answerLiker = answerLiker;
    }

    public String getAnswerLiker() 
    {
        return answerLiker;
    }
    public void setAnswerAcception(String answerAcception) 
    {
        this.answerAcception = answerAcception;
    }

    public String getAnswerAcception() 
    {
        return answerAcception;
    }
    public void setAnswerLatetime(String answerLatetime) 
    {
        this.answerLatetime = answerLatetime;
    }

    public String getAnswerLatetime() 
    {
        return answerLatetime;
    }
    public void setAnswerState(String answerState) 
    {
        this.answerState = answerState;
    }

    public String getAnswerState() 
    {
        return answerState;
    }
    public void setAnswerComment(String answerComment) 
    {
        this.answerComment = answerComment;
    }

    public String getAnswerComment() 
    {
        return answerComment;
    }
    public void setAnswerSort(Integer answerSort) 
    {
        this.answerSort = answerSort;
    }

    public Integer getAnswerSort() 
    {
        return answerSort;
    }
    public void setAnswerCourse(String answerCourse) 
    {
        this.answerCourse = answerCourse;
    }

    public String getAnswerCourse() 
    {
        return answerCourse;
    }
    public void setLastDate(String lastDate) 
    {
        this.lastDate = lastDate;
    }

    public String getLastDate() 
    {
        return lastDate;
    }
    public void setStateId(String stateId) 
    {
        this.stateId = stateId;
    }

    public String getStateId() 
    {
        return stateId;
    }
    public void setCreaterBy(String createrBy) 
    {
        this.createrBy = createrBy;
    }

    public String getCreaterBy() 
    {
        return createrBy;
    }
    public void setUpdaterName(String updaterName) 
    {
        this.updaterName = updaterName;
    }

    public String getUpdaterName() 
    {
        return updaterName;
    }
    public void setUpdaterTime(String updaterTime) 
    {
        this.updaterTime = updaterTime;
    }

    public String getUpdaterTime() 
    {
        return updaterTime;
    }
    public void setVersionNumber(String versionNumber) 
    {
        this.versionNumber = versionNumber;
    }

    public String getVersionNumber() 
    {
        return versionNumber;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }
    public void setConpanyCodee(String conpanyCodee) 
    {
        this.conpanyCodee = conpanyCodee;
    }

    public String getConpanyCodee() 
    {
        return conpanyCodee;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("answerId", getAnswerId())
            .append("answerRelated", getAnswerRelated())
            .append("answerType", getAnswerType())
            .append("answerMaintype", getAnswerMaintype())
            .append("answerDescribe", getAnswerDescribe())
            .append("answerPeople", getAnswerPeople())
            .append("answerTime", getAnswerTime())
            .append("answerNumber", getAnswerNumber())
            .append("answerTeacher", getAnswerTeacher())
            .append("answerLiker", getAnswerLiker())
            .append("answerAcception", getAnswerAcception())
            .append("answerLatetime", getAnswerLatetime())
            .append("answerState", getAnswerState())
            .append("answerComment", getAnswerComment())
            .append("answerSort", getAnswerSort())
            .append("answerCourse", getAnswerCourse())
            .append("lastDate", getLastDate())
            .append("stateId", getStateId())
            .append("createrBy", getCreaterBy())
            .append("createTime", getCreateTime())
            .append("updaterName", getUpdaterName())
            .append("updaterTime", getUpdaterTime())
            .append("versionNumber", getVersionNumber())
            .append("remarks", getRemarks())
            .append("conpanyCodee", getConpanyCodee())
            .toString();
    }
}
