package com.ruoyi.project.mooc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 mooc_course
 * 
 * @author administrator
 * @date 2020-05-06
 */
public class MoocCourse extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 课程编号 */
    private String courseId;

    /** 课程直播关联号 */
    @Excel(name = "课程直播关联号")
    private String courseLink;

    /** 上课人数 */
    @Excel(name = "上课人数")
    private Integer courseAttend;

    /** 报名人数 */
    @Excel(name = "报名人数")
    private Integer courseEntroll;

    /** 课程评分 */
    @Excel(name = "课程评分")
    private Integer courseGrade;

    /** 课程排序 */
    @Excel(name = "课程排序")
    private Integer courseSort;

    /** 上课老师 */
    @Excel(name = "上课老师")
    private String courseTeacher;

    /** 课程说明 */
    @Excel(name = "课程说明")
    private String courseExplain;

    /** 课程大类 */
    @Excel(name = "课程大类")
    private String courseBigtype;

    /** 开课时间 */
    @Excel(name = "开课时间")
    private String courseStarttime;

    /** 课程评论 */
    @Excel(name = "课程评论")
    private String courseComment;

    /** 课程名称 */
    @Excel(name = "课程名称")
    private String courseName;

    /** 课程审核人 */
    @Excel(name = "课程审核人")
    private String courseReviewed;

    /** 最近使用日期 */
    @Excel(name = "最近使用日期")
    private String lastDate;

    /** 状态 */
    @Excel(name = "状态")
    private String stateId;

    /** 创建者 */
    @Excel(name = "创建者")
    private String createrBy;

    /** 更新人 */
    @Excel(name = "更新人")
    private String updaterName;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private String updaterTime;

    /** 版本号 */
    @Excel(name = "版本号")
    private String versionNumber;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    /** 公司机构名 */
    @Excel(name = "公司机构名")
    private String conpanyCodee;

    public void setCourseId(String courseId) 
    {
        this.courseId = courseId;
    }

    public String getCourseId() 
    {
        return courseId;
    }
    public void setCourseLink(String courseLink) 
    {
        this.courseLink = courseLink;
    }

    public String getCourseLink() 
    {
        return courseLink;
    }
    public void setCourseAttend(Integer courseAttend) 
    {
        this.courseAttend = courseAttend;
    }

    public Integer getCourseAttend() 
    {
        return courseAttend;
    }
    public void setCourseEntroll(Integer courseEntroll) 
    {
        this.courseEntroll = courseEntroll;
    }

    public Integer getCourseEntroll() 
    {
        return courseEntroll;
    }
    public void setCourseGrade(Integer courseGrade) 
    {
        this.courseGrade = courseGrade;
    }

    public Integer getCourseGrade() 
    {
        return courseGrade;
    }
    public void setCourseSort(Integer courseSort) 
    {
        this.courseSort = courseSort;
    }

    public Integer getCourseSort() 
    {
        return courseSort;
    }
    public void setCourseTeacher(String courseTeacher) 
    {
        this.courseTeacher = courseTeacher;
    }

    public String getCourseTeacher() 
    {
        return courseTeacher;
    }
    public void setCourseExplain(String courseExplain) 
    {
        this.courseExplain = courseExplain;
    }

    public String getCourseExplain() 
    {
        return courseExplain;
    }
    public void setCourseBigtype(String courseBigtype) 
    {
        this.courseBigtype = courseBigtype;
    }

    public String getCourseBigtype() 
    {
        return courseBigtype;
    }
    public void setCourseStarttime(String courseStarttime) 
    {
        this.courseStarttime = courseStarttime;
    }

    public String getCourseStarttime() 
    {
        return courseStarttime;
    }
    public void setCourseComment(String courseComment) 
    {
        this.courseComment = courseComment;
    }

    public String getCourseComment() 
    {
        return courseComment;
    }
    public void setCourseName(String courseName) 
    {
        this.courseName = courseName;
    }

    public String getCourseName() 
    {
        return courseName;
    }
    public void setCourseReviewed(String courseReviewed) 
    {
        this.courseReviewed = courseReviewed;
    }

    public String getCourseReviewed() 
    {
        return courseReviewed;
    }
    public void setLastDate(String lastDate) 
    {
        this.lastDate = lastDate;
    }

    public String getLastDate() 
    {
        return lastDate;
    }
    public void setStateId(String stateId) 
    {
        this.stateId = stateId;
    }

    public String getStateId() 
    {
        return stateId;
    }
    public void setCreaterBy(String createrBy) 
    {
        this.createrBy = createrBy;
    }

    public String getCreaterBy() 
    {
        return createrBy;
    }
    public void setUpdaterName(String updaterName) 
    {
        this.updaterName = updaterName;
    }

    public String getUpdaterName() 
    {
        return updaterName;
    }
    public void setUpdaterTime(String updaterTime) 
    {
        this.updaterTime = updaterTime;
    }

    public String getUpdaterTime() 
    {
        return updaterTime;
    }
    public void setVersionNumber(String versionNumber) 
    {
        this.versionNumber = versionNumber;
    }

    public String getVersionNumber() 
    {
        return versionNumber;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }
    public void setConpanyCodee(String conpanyCodee) 
    {
        this.conpanyCodee = conpanyCodee;
    }

    public String getConpanyCodee() 
    {
        return conpanyCodee;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("courseId", getCourseId())
            .append("courseLink", getCourseLink())
            .append("courseAttend", getCourseAttend())
            .append("courseEntroll", getCourseEntroll())
            .append("courseGrade", getCourseGrade())
            .append("courseSort", getCourseSort())
            .append("courseTeacher", getCourseTeacher())
            .append("courseExplain", getCourseExplain())
            .append("courseBigtype", getCourseBigtype())
            .append("courseStarttime", getCourseStarttime())
            .append("courseComment", getCourseComment())
            .append("courseName", getCourseName())
            .append("courseReviewed", getCourseReviewed())
            .append("lastDate", getLastDate())
            .append("stateId", getStateId())
            .append("createrBy", getCreaterBy())
            .append("createTime", getCreateTime())
            .append("updaterName", getUpdaterName())
            .append("updaterTime", getUpdaterTime())
            .append("versionNumber", getVersionNumber())
            .append("remarks", getRemarks())
            .append("conpanyCodee", getConpanyCodee())
            .toString();
    }
}
