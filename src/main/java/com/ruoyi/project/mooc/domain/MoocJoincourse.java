package com.ruoyi.project.mooc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 mooc_joincourse
 * 
 * @author administrator
 * @date 2020-05-13
 */
public class MoocJoincourse extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 报名编号 */
    private String joinId;

    /** 报名关联号 */
    @Excel(name = "报名关联号")
    private String joinRelated;

    /** 报名说明 */
    @Excel(name = "报名说明")
    private String joinExpain;

    /** 报名时间 */
    @Excel(name = "报名时间")
    private String joinTime;

    /** 报名课程 */
    @Excel(name = "报名课程")
    private String joinCourse;

    /** 缴费情况 */
    @Excel(name = "缴费情况")
    private String joinMoney;

    /** 报名审核人 */
    @Excel(name = "报名审核人")
    private String joinReviewor;

    /** 报名信息关联号 */
    @Excel(name = "报名信息关联号")
    private String joinRelatednum;

    /** 报名人性别 */
    @Excel(name = "报名人性别")
    private String joinSex;

    /** 报名人住址 */
    @Excel(name = "报名人住址")
    private String joinAddress;

    /** 报名人家长姓名 */
    @Excel(name = "报名人家长姓名")
    private String joinFamilyname;

    /** 报名人家长电话 */
    @Excel(name = "报名人家长电话")
    private String joinFamilyphone;

    /** 报名人住址 */
    @Excel(name = "报名人住址")
    private String joinFamilyaddress;

    /** 报名人信息号 */
    @Excel(name = "报名人信息号")
    private String joinRelatednumber;

    /** 报名人状态 */
    @Excel(name = "报名人状态")
    private String joinState;

    /** 报名人邮箱 */
    @Excel(name = "报名人邮箱")
    private String joinMail;

    /** 报名人电话 */
    @Excel(name = "报名人电话")
    private String joinPhune;

    /** 报名人姓名 */
    @Excel(name = "报名人姓名")
    private String joinName;

    /** 报名班级 */
    @Excel(name = "报名班级")
    private String joinClass;

    /** 报名操作人 */
    @Excel(name = "报名操作人")
    private String joinOppreation;

    /** 报名名称 */
    @Excel(name = "报名名称")
    private String joinContentname;

    /** 报名依据 */
    @Excel(name = "报名依据")
    private String joinAccroding;

    /** 报名类别 */
    @Excel(name = "报名类别")
    private String joinContent;

    /** 报名大类 */
    @Excel(name = "报名大类")
    private String joinMaincontent;

    /** 报名描述 */
    @Excel(name = "报名描述")
    private String joinDescibe;

    /** 报名编号 */
    @Excel(name = "报名编号")
    private String joinNumber;

    /** 报名排序 */
    @Excel(name = "报名排序")
    private String joinSort;

    /** 报名备注 */
    @Excel(name = "报名备注")
    private String joinRemark;

    /** 最近使用日期 */
    @Excel(name = "最近使用日期")
    private String lastDate;

    /** 状态 */
    @Excel(name = "状态")
    private String stateId;

    /** 创建者 */
    @Excel(name = "创建者")
    private String createrBy;

    /** 更新人 */
    @Excel(name = "更新人")
    private String updaterName;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private String updaterTime;

    /** 版本号 */
    @Excel(name = "版本号")
    private String versionNumber;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    /** 公司机构名 */
    @Excel(name = "公司机构名")
    private String conpanyCodee;

    public void setJoinId(String joinId) 
    {
        this.joinId = joinId;
    }

    public String getJoinId() 
    {
        return joinId;
    }
    public void setJoinRelated(String joinRelated) 
    {
        this.joinRelated = joinRelated;
    }

    public String getJoinRelated() 
    {
        return joinRelated;
    }
    public void setJoinExpain(String joinExpain) 
    {
        this.joinExpain = joinExpain;
    }

    public String getJoinExpain() 
    {
        return joinExpain;
    }
    public void setJoinTime(String joinTime) 
    {
        this.joinTime = joinTime;
    }

    public String getJoinTime() 
    {
        return joinTime;
    }
    public void setJoinCourse(String joinCourse) 
    {
        this.joinCourse = joinCourse;
    }

    public String getJoinCourse() 
    {
        return joinCourse;
    }
    public void setJoinMoney(String joinMoney) 
    {
        this.joinMoney = joinMoney;
    }

    public String getJoinMoney() 
    {
        return joinMoney;
    }
    public void setJoinReviewor(String joinReviewor) 
    {
        this.joinReviewor = joinReviewor;
    }

    public String getJoinReviewor() 
    {
        return joinReviewor;
    }
    public void setJoinRelatednum(String joinRelatednum) 
    {
        this.joinRelatednum = joinRelatednum;
    }

    public String getJoinRelatednum() 
    {
        return joinRelatednum;
    }
    public void setJoinSex(String joinSex) 
    {
        this.joinSex = joinSex;
    }

    public String getJoinSex() 
    {
        return joinSex;
    }
    public void setJoinAddress(String joinAddress) 
    {
        this.joinAddress = joinAddress;
    }

    public String getJoinAddress() 
    {
        return joinAddress;
    }
    public void setJoinFamilyname(String joinFamilyname) 
    {
        this.joinFamilyname = joinFamilyname;
    }

    public String getJoinFamilyname() 
    {
        return joinFamilyname;
    }
    public void setJoinFamilyphone(String joinFamilyphone) 
    {
        this.joinFamilyphone = joinFamilyphone;
    }

    public String getJoinFamilyphone() 
    {
        return joinFamilyphone;
    }
    public void setJoinFamilyaddress(String joinFamilyaddress) 
    {
        this.joinFamilyaddress = joinFamilyaddress;
    }

    public String getJoinFamilyaddress() 
    {
        return joinFamilyaddress;
    }
    public void setJoinRelatednumber(String joinRelatednumber) 
    {
        this.joinRelatednumber = joinRelatednumber;
    }

    public String getJoinRelatednumber() 
    {
        return joinRelatednumber;
    }
    public void setJoinState(String joinState) 
    {
        this.joinState = joinState;
    }

    public String getJoinState() 
    {
        return joinState;
    }
    public void setJoinMail(String joinMail) 
    {
        this.joinMail = joinMail;
    }

    public String getJoinMail() 
    {
        return joinMail;
    }
    public void setJoinPhune(String joinPhune) 
    {
        this.joinPhune = joinPhune;
    }

    public String getJoinPhune() 
    {
        return joinPhune;
    }
    public void setJoinName(String joinName) 
    {
        this.joinName = joinName;
    }

    public String getJoinName() 
    {
        return joinName;
    }
    public void setJoinClass(String joinClass) 
    {
        this.joinClass = joinClass;
    }

    public String getJoinClass() 
    {
        return joinClass;
    }
    public void setJoinOppreation(String joinOppreation) 
    {
        this.joinOppreation = joinOppreation;
    }

    public String getJoinOppreation() 
    {
        return joinOppreation;
    }
    public void setJoinContentname(String joinContentname) 
    {
        this.joinContentname = joinContentname;
    }

    public String getJoinContentname() 
    {
        return joinContentname;
    }
    public void setJoinAccroding(String joinAccroding) 
    {
        this.joinAccroding = joinAccroding;
    }

    public String getJoinAccroding() 
    {
        return joinAccroding;
    }
    public void setJoinContent(String joinContent) 
    {
        this.joinContent = joinContent;
    }

    public String getJoinContent() 
    {
        return joinContent;
    }
    public void setJoinMaincontent(String joinMaincontent) 
    {
        this.joinMaincontent = joinMaincontent;
    }

    public String getJoinMaincontent() 
    {
        return joinMaincontent;
    }
    public void setJoinDescibe(String joinDescibe) 
    {
        this.joinDescibe = joinDescibe;
    }

    public String getJoinDescibe() 
    {
        return joinDescibe;
    }
    public void setJoinNumber(String joinNumber) 
    {
        this.joinNumber = joinNumber;
    }

    public String getJoinNumber() 
    {
        return joinNumber;
    }
    public void setJoinSort(String joinSort) 
    {
        this.joinSort = joinSort;
    }

    public String getJoinSort() 
    {
        return joinSort;
    }
    public void setJoinRemark(String joinRemark) 
    {
        this.joinRemark = joinRemark;
    }

    public String getJoinRemark() 
    {
        return joinRemark;
    }
    public void setLastDate(String lastDate) 
    {
        this.lastDate = lastDate;
    }

    public String getLastDate() 
    {
        return lastDate;
    }
    public void setStateId(String stateId) 
    {
        this.stateId = stateId;
    }

    public String getStateId() 
    {
        return stateId;
    }
    public void setCreaterBy(String createrBy) 
    {
        this.createrBy = createrBy;
    }

    public String getCreaterBy() 
    {
        return createrBy;
    }
    public void setUpdaterName(String updaterName) 
    {
        this.updaterName = updaterName;
    }

    public String getUpdaterName() 
    {
        return updaterName;
    }
    public void setUpdaterTime(String updaterTime) 
    {
        this.updaterTime = updaterTime;
    }

    public String getUpdaterTime() 
    {
        return updaterTime;
    }
    public void setVersionNumber(String versionNumber) 
    {
        this.versionNumber = versionNumber;
    }

    public String getVersionNumber() 
    {
        return versionNumber;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }
    public void setConpanyCodee(String conpanyCodee) 
    {
        this.conpanyCodee = conpanyCodee;
    }

    public String getConpanyCodee() 
    {
        return conpanyCodee;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("joinId", getJoinId())
            .append("joinRelated", getJoinRelated())
            .append("joinExpain", getJoinExpain())
            .append("joinTime", getJoinTime())
            .append("joinCourse", getJoinCourse())
            .append("joinMoney", getJoinMoney())
            .append("joinReviewor", getJoinReviewor())
            .append("joinRelatednum", getJoinRelatednum())
            .append("joinSex", getJoinSex())
            .append("joinAddress", getJoinAddress())
            .append("joinFamilyname", getJoinFamilyname())
            .append("joinFamilyphone", getJoinFamilyphone())
            .append("joinFamilyaddress", getJoinFamilyaddress())
            .append("joinRelatednumber", getJoinRelatednumber())
            .append("joinState", getJoinState())
            .append("joinMail", getJoinMail())
            .append("joinPhune", getJoinPhune())
            .append("joinName", getJoinName())
            .append("joinClass", getJoinClass())
            .append("joinOppreation", getJoinOppreation())
            .append("joinContentname", getJoinContentname())
            .append("joinAccroding", getJoinAccroding())
            .append("joinContent", getJoinContent())
            .append("joinMaincontent", getJoinMaincontent())
            .append("joinDescibe", getJoinDescibe())
            .append("joinNumber", getJoinNumber())
            .append("joinSort", getJoinSort())
            .append("joinRemark", getJoinRemark())
            .append("lastDate", getLastDate())
            .append("stateId", getStateId())
            .append("createrBy", getCreaterBy())
            .append("createTime", getCreateTime())
            .append("updaterName", getUpdaterName())
            .append("updaterTime", getUpdaterTime())
            .append("versionNumber", getVersionNumber())
            .append("remarks", getRemarks())
            .append("conpanyCodee", getConpanyCodee())
            .toString();
    }
}
