package com.ruoyi.project.mooc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 mooc_directlive
 * 
 * @author administrator
 * @date 2020-05-06
 */
public class MoocDirectlive extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 视频主键 */
    private Long videoId;

    /** 主键字符 */
    private String videoIdkey;

    /** 视频分享秘钥 */
    @Excel(name = "视频分享秘钥")
    private String vodeoIdkey;

    /** 视频名称 */
    @Excel(name = "视频名称")
    private String videoName;

    /** 视频类型 */
    @Excel(name = "视频类型")
    private String videoType;

    /** 视频地址 */
    @Excel(name = "视频地址")
    private String videoAddress;

    /** 视频存储位置 */
    @Excel(name = "视频存储位置")
    private String videoLoad;

    /** 视频章节 */
    @Excel(name = "视频章节")
    private String videoContent;

    /** 直播地址 */
    @Excel(name = "直播地址")
    private String videoOpadress;

    /** 视频权限 */
    @Excel(name = "视频权限")
    private String videoAuthority;

    /** 视频收费 */
    @Excel(name = "视频收费")
    private String videoMony;

    /** 视频参数 */
    @Excel(name = "视频参数")
    private String videoDetil;

    /** 视频码率 */
    @Excel(name = "视频码率")
    private String videoNum;

    /** 视频板块 */
    @Excel(name = "视频板块")
    private String videoCon;

    /** 视频大小 */
    @Excel(name = "视频大小")
    private String videoSize;

    /** 状态（0正常 1禁用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=禁用")
    private String status;

    /** 上传时间 */
    @Excel(name = "上传时间")
    private String upTime;

    /** 上传备注 */
    @Excel(name = "上传备注")
    private String upRemark;

    /** 上传分类 */
    @Excel(name = "上传分类")
    private String upType;

    /** 视频简介 */
    @Excel(name = "视频简介")
    private String videoIntroduction;

    /** 视频图片地址 */
    @Excel(name = "视频图片地址")
    private String videoPic;

    /** 封面图片类型 */
    @Excel(name = "封面图片类型")
    private String videoPictype;

    /** 清晰度 */
    @Excel(name = "清晰度")
    private String videoQulity;

    /** 视频时长 */
    @Excel(name = "视频时长")
    private String videoPlaytime;

    /** 点赞次数 */
    @Excel(name = "点赞次数")
    private String pointsCount;

    /** 评论次数 */
    @Excel(name = "评论次数")
    private String commentFrequency;

    /** 排行 */
    @Excel(name = "排行")
    private String vodeoSort;

    /** 下载次数 */
    @Excel(name = "下载次数")
    private String downloadCount;

    /** 收藏次数 */
    @Excel(name = "收藏次数")
    private String storeCount;

    /** 观看次数 */
    @Excel(name = "观看次数")
    private String playCount;

    /** 备用1 */
    @Excel(name = "备用1")
    private String column39;

    /** 备用2 */
    @Excel(name = "备用2")
    private String column38;

    /** 备用3 */
    @Excel(name = "备用3")
    private String column37;

    /** 备用4 */
    @Excel(name = "备用4")
    private String column36;

    /** 备用5 */
    @Excel(name = "备用5")
    private String column35;

    public void setVideoId(Long videoId) 
    {
        this.videoId = videoId;
    }

    public Long getVideoId() 
    {
        return videoId;
    }
    public void setVideoIdkey(String videoIdkey) 
    {
        this.videoIdkey = videoIdkey;
    }

    public String getVideoIdkey() 
    {
        return videoIdkey;
    }
    public void setVodeoIdkey(String vodeoIdkey) 
    {
        this.vodeoIdkey = vodeoIdkey;
    }

    public String getVodeoIdkey() 
    {
        return vodeoIdkey;
    }
    public void setVideoName(String videoName) 
    {
        this.videoName = videoName;
    }

    public String getVideoName() 
    {
        return videoName;
    }
    public void setVideoType(String videoType) 
    {
        this.videoType = videoType;
    }

    public String getVideoType() 
    {
        return videoType;
    }
    public void setVideoAddress(String videoAddress) 
    {
        this.videoAddress = videoAddress;
    }

    public String getVideoAddress() 
    {
        return videoAddress;
    }
    public void setVideoLoad(String videoLoad) 
    {
        this.videoLoad = videoLoad;
    }

    public String getVideoLoad() 
    {
        return videoLoad;
    }
    public void setVideoContent(String videoContent) 
    {
        this.videoContent = videoContent;
    }

    public String getVideoContent() 
    {
        return videoContent;
    }
    public void setVideoOpadress(String videoOpadress) 
    {
        this.videoOpadress = videoOpadress;
    }

    public String getVideoOpadress() 
    {
        return videoOpadress;
    }
    public void setVideoAuthority(String videoAuthority) 
    {
        this.videoAuthority = videoAuthority;
    }

    public String getVideoAuthority() 
    {
        return videoAuthority;
    }
    public void setVideoMony(String videoMony) 
    {
        this.videoMony = videoMony;
    }

    public String getVideoMony() 
    {
        return videoMony;
    }
    public void setVideoDetil(String videoDetil) 
    {
        this.videoDetil = videoDetil;
    }

    public String getVideoDetil() 
    {
        return videoDetil;
    }
    public void setVideoNum(String videoNum) 
    {
        this.videoNum = videoNum;
    }

    public String getVideoNum() 
    {
        return videoNum;
    }
    public void setVideoCon(String videoCon) 
    {
        this.videoCon = videoCon;
    }

    public String getVideoCon() 
    {
        return videoCon;
    }
    public void setVideoSize(String videoSize) 
    {
        this.videoSize = videoSize;
    }

    public String getVideoSize() 
    {
        return videoSize;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setUpTime(String upTime) 
    {
        this.upTime = upTime;
    }

    public String getUpTime() 
    {
        return upTime;
    }
    public void setUpRemark(String upRemark) 
    {
        this.upRemark = upRemark;
    }

    public String getUpRemark() 
    {
        return upRemark;
    }
    public void setUpType(String upType) 
    {
        this.upType = upType;
    }

    public String getUpType() 
    {
        return upType;
    }
    public void setVideoIntroduction(String videoIntroduction) 
    {
        this.videoIntroduction = videoIntroduction;
    }

    public String getVideoIntroduction() 
    {
        return videoIntroduction;
    }
    public void setVideoPic(String videoPic) 
    {
        this.videoPic = videoPic;
    }

    public String getVideoPic() 
    {
        return videoPic;
    }
    public void setVideoPictype(String videoPictype) 
    {
        this.videoPictype = videoPictype;
    }

    public String getVideoPictype() 
    {
        return videoPictype;
    }
    public void setVideoQulity(String videoQulity) 
    {
        this.videoQulity = videoQulity;
    }

    public String getVideoQulity() 
    {
        return videoQulity;
    }
    public void setVideoPlaytime(String videoPlaytime) 
    {
        this.videoPlaytime = videoPlaytime;
    }

    public String getVideoPlaytime() 
    {
        return videoPlaytime;
    }
    public void setPointsCount(String pointsCount) 
    {
        this.pointsCount = pointsCount;
    }

    public String getPointsCount() 
    {
        return pointsCount;
    }
    public void setCommentFrequency(String commentFrequency) 
    {
        this.commentFrequency = commentFrequency;
    }

    public String getCommentFrequency() 
    {
        return commentFrequency;
    }
    public void setVodeoSort(String vodeoSort) 
    {
        this.vodeoSort = vodeoSort;
    }

    public String getVodeoSort() 
    {
        return vodeoSort;
    }
    public void setDownloadCount(String downloadCount) 
    {
        this.downloadCount = downloadCount;
    }

    public String getDownloadCount() 
    {
        return downloadCount;
    }
    public void setStoreCount(String storeCount) 
    {
        this.storeCount = storeCount;
    }

    public String getStoreCount() 
    {
        return storeCount;
    }
    public void setPlayCount(String playCount) 
    {
        this.playCount = playCount;
    }

    public String getPlayCount() 
    {
        return playCount;
    }
    public void setColumn39(String column39) 
    {
        this.column39 = column39;
    }

    public String getColumn39() 
    {
        return column39;
    }
    public void setColumn38(String column38) 
    {
        this.column38 = column38;
    }

    public String getColumn38() 
    {
        return column38;
    }
    public void setColumn37(String column37) 
    {
        this.column37 = column37;
    }

    public String getColumn37() 
    {
        return column37;
    }
    public void setColumn36(String column36) 
    {
        this.column36 = column36;
    }

    public String getColumn36() 
    {
        return column36;
    }
    public void setColumn35(String column35) 
    {
        this.column35 = column35;
    }

    public String getColumn35() 
    {
        return column35;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("videoId", getVideoId())
            .append("videoIdkey", getVideoIdkey())
            .append("vodeoIdkey", getVodeoIdkey())
            .append("videoName", getVideoName())
            .append("videoType", getVideoType())
            .append("videoAddress", getVideoAddress())
            .append("videoLoad", getVideoLoad())
            .append("videoContent", getVideoContent())
            .append("videoOpadress", getVideoOpadress())
            .append("videoAuthority", getVideoAuthority())
            .append("videoMony", getVideoMony())
            .append("videoDetil", getVideoDetil())
            .append("videoNum", getVideoNum())
            .append("videoCon", getVideoCon())
            .append("videoSize", getVideoSize())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("upTime", getUpTime())
            .append("upRemark", getUpRemark())
            .append("upType", getUpType())
            .append("videoIntroduction", getVideoIntroduction())
            .append("videoPic", getVideoPic())
            .append("videoPictype", getVideoPictype())
            .append("videoQulity", getVideoQulity())
            .append("videoPlaytime", getVideoPlaytime())
            .append("pointsCount", getPointsCount())
            .append("commentFrequency", getCommentFrequency())
            .append("vodeoSort", getVodeoSort())
            .append("downloadCount", getDownloadCount())
            .append("storeCount", getStoreCount())
            .append("playCount", getPlayCount())
            .append("column39", getColumn39())
            .append("column38", getColumn38())
            .append("column37", getColumn37())
            .append("column36", getColumn36())
            .append("column35", getColumn35())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
