package com.ruoyi.project.mooc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 mooc_serial_number
 * 
 * @author administrator
 * @date 2020-04-26
 */
public class MoocSerialNumber extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 单号名称 */
    private String idName;

    /** 单号说明 */
    @Excel(name = "单号说明")
    private String idDesc;

    /** 固定格式 */
    @Excel(name = "固定格式")
    private String ruleStr;

    /** 日期格式 */
    @Excel(name = "日期格式")
    private String dateFormat;

    /** 流水号位数 */
    @Excel(name = "流水号位数")
    private Integer seqLength;

    /** 当前流水 */
    @Excel(name = "当前流水")
    private Long curSeqId;

    /** 最近使用日期 */
    @Excel(name = "最近使用日期")
    private String lastDate;

    /** 状态 */
    @Excel(name = "状态")
    private String stateId;

    /** 更新者 */
    @Excel(name = "更新者")
    private String createrBy;

    /** 更新人 */
    @Excel(name = "更新人")
    private String updaterName;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private String updaterTime;

    /** 版本号 */
    @Excel(name = "版本号")
    private String versionNumber;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    /** 公司机构名 */
    @Excel(name = "公司机构名")
    private String conpanyCodee;

    public void setIdName(String idName) 
    {
        this.idName = idName;
    }

    public String getIdName() 
    {
        return idName;
    }
    public void setIdDesc(String idDesc) 
    {
        this.idDesc = idDesc;
    }

    public String getIdDesc() 
    {
        return idDesc;
    }
    public void setRuleStr(String ruleStr) 
    {
        this.ruleStr = ruleStr;
    }

    public String getRuleStr() 
    {
        return ruleStr;
    }
    public void setDateFormat(String dateFormat) 
    {
        this.dateFormat = dateFormat;
    }

    public String getDateFormat() 
    {
        return dateFormat;
    }
    public void setSeqLength(Integer seqLength) 
    {
        this.seqLength = seqLength;
    }

    public Integer getSeqLength() 
    {
        return seqLength;
    }
    public void setCurSeqId(Long curSeqId) 
    {
        this.curSeqId = curSeqId;
    }

    public Long getCurSeqId() 
    {
        return curSeqId;
    }
    public void setLastDate(String lastDate) 
    {
        this.lastDate = lastDate;
    }

    public String getLastDate() 
    {
        return lastDate;
    }
    public void setStateId(String stateId) 
    {
        this.stateId = stateId;
    }

    public String getStateId() 
    {
        return stateId;
    }
    public void setCreaterBy(String createrBy) 
    {
        this.createrBy = createrBy;
    }

    public String getCreaterBy() 
    {
        return createrBy;
    }
    public void setUpdaterName(String updaterName) 
    {
        this.updaterName = updaterName;
    }

    public String getUpdaterName() 
    {
        return updaterName;
    }
    public void setUpdaterTime(String updaterTime) 
    {
        this.updaterTime = updaterTime;
    }

    public String getUpdaterTime() 
    {
        return updaterTime;
    }
    public void setVersionNumber(String versionNumber) 
    {
        this.versionNumber = versionNumber;
    }

    public String getVersionNumber() 
    {
        return versionNumber;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }
    public void setConpanyCodee(String conpanyCodee) 
    {
        this.conpanyCodee = conpanyCodee;
    }

    public String getConpanyCodee() 
    {
        return conpanyCodee;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idName", getIdName())
            .append("idDesc", getIdDesc())
            .append("ruleStr", getRuleStr())
            .append("dateFormat", getDateFormat())
            .append("seqLength", getSeqLength())
            .append("curSeqId", getCurSeqId())
            .append("lastDate", getLastDate())
            .append("stateId", getStateId())
            .append("createrBy", getCreaterBy())
            .append("createTime", getCreateTime())
            .append("updaterName", getUpdaterName())
            .append("updaterTime", getUpdaterTime())
            .append("versionNumber", getVersionNumber())
            .append("remarks", getRemarks())
            .append("conpanyCodee", getConpanyCodee())
            .toString();
    }
}
