package com.ruoyi.project.mooc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 mooc_question
 * 
 * @author administrator
 * @date 2020-05-06
 */
public class MoocQuestion extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 问题编号 */
    private String questionId;

    /** 问题答案关联号 */
    @Excel(name = "问题答案关联号")
    private String questionRelated;

    /** 问题类型 */
    @Excel(name = "问题类型")
    private String questionType;

    /** 问题大类 */
    @Excel(name = "问题大类")
    private String questionMaintype;

    /** 问题描述 */
    @Excel(name = "问题描述")
    private String questionDescribe;

    /** 提问人 */
    @Excel(name = "提问人")
    private String questionPeople;

    /** 提问时间 */
    @Excel(name = "提问时间")
    private String questionTime;

    /** 答案编号 */
    @Excel(name = "答案编号")
    private String questionNumber;

    /** 解答老师 */
    @Excel(name = "解答老师")
    private String questionAnswerteacher;

    /** 提问次数 */
    @Excel(name = "提问次数")
    private String questionFrequency;

    /** 答案采纳率 */
    @Excel(name = "答案采纳率")
    private String questionAchive;

    /** 最迟回答时间 */
    @Excel(name = "最迟回答时间")
    private String questionLatetime;

    /** 问题处理状态 */
    @Excel(name = "问题处理状态")
    private String questionState;

    /** 问题评论 */
    @Excel(name = "问题评论")
    private String questionComment;

    /** 问题排序 */
    @Excel(name = "问题排序")
    private Integer questionSort;

    /** 问题关联课程 */
    @Excel(name = "问题关联课程")
    private String questionCoursw;

    /** 最近使用日期 */
    @Excel(name = "最近使用日期")
    private String lastDate;

    /** 状态 */
    @Excel(name = "状态")
    private String stateId;

    /** 创建者 */
    @Excel(name = "创建者")
    private String createrBy;

    /** 更新人 */
    @Excel(name = "更新人")
    private String updaterName;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private String updaterTime;

    /** 版本号 */
    @Excel(name = "版本号")
    private String versionNumber;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    /** 公司机构名 */
    @Excel(name = "公司机构名")
    private String conpanyCodee;

    public void setQuestionId(String questionId) 
    {
        this.questionId = questionId;
    }

    public String getQuestionId() 
    {
        return questionId;
    }
    public void setQuestionRelated(String questionRelated) 
    {
        this.questionRelated = questionRelated;
    }

    public String getQuestionRelated() 
    {
        return questionRelated;
    }
    public void setQuestionType(String questionType) 
    {
        this.questionType = questionType;
    }

    public String getQuestionType() 
    {
        return questionType;
    }
    public void setQuestionMaintype(String questionMaintype) 
    {
        this.questionMaintype = questionMaintype;
    }

    public String getQuestionMaintype() 
    {
        return questionMaintype;
    }
    public void setQuestionDescribe(String questionDescribe) 
    {
        this.questionDescribe = questionDescribe;
    }

    public String getQuestionDescribe() 
    {
        return questionDescribe;
    }
    public void setQuestionPeople(String questionPeople) 
    {
        this.questionPeople = questionPeople;
    }

    public String getQuestionPeople() 
    {
        return questionPeople;
    }
    public void setQuestionTime(String questionTime) 
    {
        this.questionTime = questionTime;
    }

    public String getQuestionTime() 
    {
        return questionTime;
    }
    public void setQuestionNumber(String questionNumber) 
    {
        this.questionNumber = questionNumber;
    }

    public String getQuestionNumber() 
    {
        return questionNumber;
    }
    public void setQuestionAnswerteacher(String questionAnswerteacher) 
    {
        this.questionAnswerteacher = questionAnswerteacher;
    }

    public String getQuestionAnswerteacher() 
    {
        return questionAnswerteacher;
    }
    public void setQuestionFrequency(String questionFrequency) 
    {
        this.questionFrequency = questionFrequency;
    }

    public String getQuestionFrequency() 
    {
        return questionFrequency;
    }
    public void setQuestionAchive(String questionAchive) 
    {
        this.questionAchive = questionAchive;
    }

    public String getQuestionAchive() 
    {
        return questionAchive;
    }
    public void setQuestionLatetime(String questionLatetime) 
    {
        this.questionLatetime = questionLatetime;
    }

    public String getQuestionLatetime() 
    {
        return questionLatetime;
    }
    public void setQuestionState(String questionState) 
    {
        this.questionState = questionState;
    }

    public String getQuestionState() 
    {
        return questionState;
    }
    public void setQuestionComment(String questionComment) 
    {
        this.questionComment = questionComment;
    }

    public String getQuestionComment() 
    {
        return questionComment;
    }
    public void setQuestionSort(Integer questionSort) 
    {
        this.questionSort = questionSort;
    }

    public Integer getQuestionSort() 
    {
        return questionSort;
    }
    public void setQuestionCoursw(String questionCoursw) 
    {
        this.questionCoursw = questionCoursw;
    }

    public String getQuestionCoursw() 
    {
        return questionCoursw;
    }
    public void setLastDate(String lastDate) 
    {
        this.lastDate = lastDate;
    }

    public String getLastDate() 
    {
        return lastDate;
    }
    public void setStateId(String stateId) 
    {
        this.stateId = stateId;
    }

    public String getStateId() 
    {
        return stateId;
    }
    public void setCreaterBy(String createrBy) 
    {
        this.createrBy = createrBy;
    }

    public String getCreaterBy() 
    {
        return createrBy;
    }
    public void setUpdaterName(String updaterName) 
    {
        this.updaterName = updaterName;
    }

    public String getUpdaterName() 
    {
        return updaterName;
    }
    public void setUpdaterTime(String updaterTime) 
    {
        this.updaterTime = updaterTime;
    }

    public String getUpdaterTime() 
    {
        return updaterTime;
    }
    public void setVersionNumber(String versionNumber) 
    {
        this.versionNumber = versionNumber;
    }

    public String getVersionNumber() 
    {
        return versionNumber;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }
    public void setConpanyCodee(String conpanyCodee) 
    {
        this.conpanyCodee = conpanyCodee;
    }

    public String getConpanyCodee() 
    {
        return conpanyCodee;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("questionId", getQuestionId())
            .append("questionRelated", getQuestionRelated())
            .append("questionType", getQuestionType())
            .append("questionMaintype", getQuestionMaintype())
            .append("questionDescribe", getQuestionDescribe())
            .append("questionPeople", getQuestionPeople())
            .append("questionTime", getQuestionTime())
            .append("questionNumber", getQuestionNumber())
            .append("questionAnswerteacher", getQuestionAnswerteacher())
            .append("questionFrequency", getQuestionFrequency())
            .append("questionAchive", getQuestionAchive())
            .append("questionLatetime", getQuestionLatetime())
            .append("questionState", getQuestionState())
            .append("questionComment", getQuestionComment())
            .append("questionSort", getQuestionSort())
            .append("questionCoursw", getQuestionCoursw())
            .append("lastDate", getLastDate())
            .append("stateId", getStateId())
            .append("createrBy", getCreaterBy())
            .append("createTime", getCreateTime())
            .append("updaterName", getUpdaterName())
            .append("updaterTime", getUpdaterTime())
            .append("versionNumber", getVersionNumber())
            .append("remarks", getRemarks())
            .append("conpanyCodee", getConpanyCodee())
            .toString();
    }
}
