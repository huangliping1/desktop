package com.ruoyi.project.mooc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 mooc_updatefile
 * 
 * @author administrator
 * @date 2020-05-13
 */
public class MoocUpdatefile extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 文件编号 */
    private String idName;

    /** 原文件名 */
    @Excel(name = "原文件名")
    private String fileOriginalname;

    /** 文件说明 */
    @Excel(name = "文件说明")
    private String idDesc;

    /** 文件所属大类 */
    @Excel(name = "文件所属大类")
    private String fileOrdertype;

    /** 文件格式 */
    @Excel(name = "文件格式")
    private String fileFormat;

    /** 日期格式 */
    @Excel(name = "日期格式")
    private String dateFormat;

    /** 文件大小 */
    @Excel(name = "文件大小")
    private String fileLength;

    /** 下载次数 */
    @Excel(name = "下载次数")
    private String fileFerequency;

    /** 最近使用日期 */
    @Excel(name = "最近使用日期")
    private String lastDate;

    /** 状态 */
    @Excel(name = "状态")
    private String stateId;

    /** 创建者 */
    @Excel(name = "创建者")
    private String createrBy;

    /** 更新人 */
    @Excel(name = "更新人")
    private String updaterName;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private String updaterTime;

    /** 版本号 */
    @Excel(name = "版本号")
    private String versionNumber;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    /** 公司机构名 */
    @Excel(name = "公司机构名")
    private String conpanyCodee;

    public void setIdName(String idName) 
    {
        this.idName = idName;
    }

    public String getIdName() 
    {
        return idName;
    }
    public void setFileOriginalname(String fileOriginalname) 
    {
        this.fileOriginalname = fileOriginalname;
    }

    public String getFileOriginalname() 
    {
        return fileOriginalname;
    }
    public void setIdDesc(String idDesc) 
    {
        this.idDesc = idDesc;
    }

    public String getIdDesc() 
    {
        return idDesc;
    }
    public void setFileOrdertype(String fileOrdertype) 
    {
        this.fileOrdertype = fileOrdertype;
    }

    public String getFileOrdertype() 
    {
        return fileOrdertype;
    }
    public void setFileFormat(String fileFormat) 
    {
        this.fileFormat = fileFormat;
    }

    public String getFileFormat() 
    {
        return fileFormat;
    }
    public void setDateFormat(String dateFormat) 
    {
        this.dateFormat = dateFormat;
    }

    public String getDateFormat() 
    {
        return dateFormat;
    }
    public void setFileLength(String string) 
    {
        this.fileLength = string;
    }

    public String getFileLength() 
    {
        return fileLength;
    }
    public void setFileFerequency(String string) 
    {
        this.fileFerequency = string;
    }

    public String getFileFerequency() 
    {
        return fileFerequency;
    }
    public void setLastDate(String lastDate) 
    {
        this.lastDate = lastDate;
    }

    public String getLastDate() 
    {
        return lastDate;
    }
    public void setStateId(String stateId) 
    {
        this.stateId = stateId;
    }

    public String getStateId() 
    {
        return stateId;
    }
    public void setCreaterBy(String createrBy) 
    {
        this.createrBy = createrBy;
    }

    public String getCreaterBy() 
    {
        return createrBy;
    }
    public void setUpdaterName(String updaterName) 
    {
        this.updaterName = updaterName;
    }

    public String getUpdaterName() 
    {
        return updaterName;
    }
    public void setUpdaterTime(String updaterTime) 
    {
        this.updaterTime = updaterTime;
    }

    public String getUpdaterTime() 
    {
        return updaterTime;
    }
    public void setVersionNumber(String versionNumber) 
    {
        this.versionNumber = versionNumber;
    }

    public String getVersionNumber() 
    {
        return versionNumber;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }
    public void setConpanyCodee(String conpanyCodee) 
    {
        this.conpanyCodee = conpanyCodee;
    }

    public String getConpanyCodee() 
    {
        return conpanyCodee;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idName", getIdName())
            .append("fileOriginalname", getFileOriginalname())
            .append("idDesc", getIdDesc())
            .append("fileOrdertype", getFileOrdertype())
            .append("fileFormat", getFileFormat())
            .append("dateFormat", getDateFormat())
            .append("fileLength", getFileLength())
            .append("fileFerequency", getFileFerequency())
            .append("lastDate", getLastDate())
            .append("stateId", getStateId())
            .append("createrBy", getCreaterBy())
            .append("createTime", getCreateTime())
            .append("updaterName", getUpdaterName())
            .append("updaterTime", getUpdaterTime())
            .append("versionNumber", getVersionNumber())
            .append("remarks", getRemarks())
            .append("conpanyCodee", getConpanyCodee())
            .toString();
    }
}
