package com.ruoyi.project.mooc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 mooc_studentcourse
 * 
 * @author administrator
 * @date 2020-05-06
 */
public class MoocStudentcourse extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 课程编号 */
    private String coursetableId;

    /** 课程直播关联号 */
    @Excel(name = "课程直播关联号")
    private String coursetableRelated;

    /** 上课时间 */
    @Excel(name = "上课时间")
    private String coursetableTime;

    /** 班级号 */
    @Excel(name = "班级号")
    private String coursetableSlassid;

    /** 上课人数 */
    @Excel(name = "上课人数")
    private String coursetableTpeople;

    /** 开课老师 */
    @Excel(name = "开课老师")
    private String coursetableTeacher;

    /** 课程内容 */
    @Excel(name = "课程内容")
    private String coursetableTcontant;

    /** 课程分类 */
    @Excel(name = "课程分类")
    private String coursetableType;

    /** 课程简介 */
    @Excel(name = "课程简介")
    private String coursetableDescription;

    /** 开课时间 */
    @Excel(name = "开课时间")
    private String coursetableStarttime;

    /** 课程评论 */
    @Excel(name = "课程评论")
    private String coursetableComment;

    /** 课程名称 */
    @Excel(name = "课程名称")
    private String coursetableName;

    /** 课程排序 */
    @Excel(name = "课程排序")
    private String coursetableSort;

    /** 报名人数 */
    @Excel(name = "报名人数")
    private String coursetableEnroll;

    /** 最近使用日期 */
    @Excel(name = "最近使用日期")
    private String lastDate;

    /** 状态 */
    @Excel(name = "状态")
    private String stateId;

    /** 创建者 */
    @Excel(name = "创建者")
    private String createrBy;

    /** 更新人 */
    @Excel(name = "更新人")
    private String updaterName;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private String updaterTime;

    /** 版本号 */
    @Excel(name = "版本号")
    private String versionNumber;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    /** 公司机构名 */
    @Excel(name = "公司机构名")
    private String conpanyCodee;

    public void setCoursetableId(String coursetableId) 
    {
        this.coursetableId = coursetableId;
    }

    public String getCoursetableId() 
    {
        return coursetableId;
    }
    public void setCoursetableRelated(String coursetableRelated) 
    {
        this.coursetableRelated = coursetableRelated;
    }

    public String getCoursetableRelated() 
    {
        return coursetableRelated;
    }
    public void setCoursetableTime(String coursetableTime) 
    {
        this.coursetableTime = coursetableTime;
    }

    public String getCoursetableTime() 
    {
        return coursetableTime;
    }
    public void setCoursetableSlassid(String coursetableSlassid) 
    {
        this.coursetableSlassid = coursetableSlassid;
    }

    public String getCoursetableSlassid() 
    {
        return coursetableSlassid;
    }
    public void setCoursetableTpeople(String coursetableTpeople) 
    {
        this.coursetableTpeople = coursetableTpeople;
    }

    public String getCoursetableTpeople() 
    {
        return coursetableTpeople;
    }
    public void setCoursetableTeacher(String coursetableTeacher) 
    {
        this.coursetableTeacher = coursetableTeacher;
    }

    public String getCoursetableTeacher() 
    {
        return coursetableTeacher;
    }
    public void setCoursetableTcontant(String coursetableTcontant) 
    {
        this.coursetableTcontant = coursetableTcontant;
    }

    public String getCoursetableTcontant() 
    {
        return coursetableTcontant;
    }
    public void setCoursetableType(String coursetableType) 
    {
        this.coursetableType = coursetableType;
    }

    public String getCoursetableType() 
    {
        return coursetableType;
    }
    public void setCoursetableDescription(String coursetableDescription) 
    {
        this.coursetableDescription = coursetableDescription;
    }

    public String getCoursetableDescription() 
    {
        return coursetableDescription;
    }
    public void setCoursetableStarttime(String coursetableStarttime) 
    {
        this.coursetableStarttime = coursetableStarttime;
    }

    public String getCoursetableStarttime() 
    {
        return coursetableStarttime;
    }
    public void setCoursetableComment(String coursetableComment) 
    {
        this.coursetableComment = coursetableComment;
    }

    public String getCoursetableComment() 
    {
        return coursetableComment;
    }
    public void setCoursetableName(String coursetableName) 
    {
        this.coursetableName = coursetableName;
    }

    public String getCoursetableName() 
    {
        return coursetableName;
    }
    public void setCoursetableSort(String coursetableSort) 
    {
        this.coursetableSort = coursetableSort;
    }

    public String getCoursetableSort() 
    {
        return coursetableSort;
    }
    public void setCoursetableEnroll(String coursetableEnroll) 
    {
        this.coursetableEnroll = coursetableEnroll;
    }

    public String getCoursetableEnroll() 
    {
        return coursetableEnroll;
    }
    public void setLastDate(String lastDate) 
    {
        this.lastDate = lastDate;
    }

    public String getLastDate() 
    {
        return lastDate;
    }
    public void setStateId(String stateId) 
    {
        this.stateId = stateId;
    }

    public String getStateId() 
    {
        return stateId;
    }
    public void setCreaterBy(String createrBy) 
    {
        this.createrBy = createrBy;
    }

    public String getCreaterBy() 
    {
        return createrBy;
    }
    public void setUpdaterName(String updaterName) 
    {
        this.updaterName = updaterName;
    }

    public String getUpdaterName() 
    {
        return updaterName;
    }
    public void setUpdaterTime(String updaterTime) 
    {
        this.updaterTime = updaterTime;
    }

    public String getUpdaterTime() 
    {
        return updaterTime;
    }
    public void setVersionNumber(String versionNumber) 
    {
        this.versionNumber = versionNumber;
    }

    public String getVersionNumber() 
    {
        return versionNumber;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }
    public void setConpanyCodee(String conpanyCodee) 
    {
        this.conpanyCodee = conpanyCodee;
    }

    public String getConpanyCodee() 
    {
        return conpanyCodee;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("coursetableId", getCoursetableId())
            .append("coursetableRelated", getCoursetableRelated())
            .append("coursetableTime", getCoursetableTime())
            .append("coursetableSlassid", getCoursetableSlassid())
            .append("coursetableTpeople", getCoursetableTpeople())
            .append("coursetableTeacher", getCoursetableTeacher())
            .append("coursetableTcontant", getCoursetableTcontant())
            .append("coursetableType", getCoursetableType())
            .append("coursetableDescription", getCoursetableDescription())
            .append("coursetableStarttime", getCoursetableStarttime())
            .append("coursetableComment", getCoursetableComment())
            .append("coursetableName", getCoursetableName())
            .append("coursetableSort", getCoursetableSort())
            .append("coursetableEnroll", getCoursetableEnroll())
            .append("lastDate", getLastDate())
            .append("stateId", getStateId())
            .append("createrBy", getCreaterBy())
            .append("createTime", getCreateTime())
            .append("updaterName", getUpdaterName())
            .append("updaterTime", getUpdaterTime())
            .append("versionNumber", getVersionNumber())
            .append("remarks", getRemarks())
            .append("conpanyCodee", getConpanyCodee())
            .toString();
    }
}
