package com.ruoyi.project.mooc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 mooc_course_content
 * 
 * @author administrator
 * @date 2020-05-06
 */
public class MoocCourseContent extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 类型编号 */
    private String typeId;

    /** 类型名称 */
    @Excel(name = "类型名称")
    private String typeName;

    /** 类型说明 */
    @Excel(name = "类型说明")
    private String typeExplain;

    /** 排序 */
    @Excel(name = "排序")
    private Integer typeSort;

    /** 类型简称 */
    @Excel(name = "类型简称")
    private String typeShort;

    /** 子类个数 */
    @Excel(name = "子类个数")
    private String typeBranchnum;

    /** 备注 */
    @Excel(name = "备注")
    private String typeRemark;

    /** 最近使用日期 */
    @Excel(name = "最近使用日期")
    private String lastDate;

    /** 状态 */
    @Excel(name = "状态")
    private String stateId;

    /** 创建者 */
    @Excel(name = "创建者")
    private String createrBy;

    /** 更新人 */
    @Excel(name = "更新人")
    private String updaterName;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private String updaterTime;

    /** 版本号 */
    @Excel(name = "版本号")
    private String versionNumber;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    /** 公司机构名 */
    @Excel(name = "公司机构名")
    private String conpanyCodee;

    public void setTypeId(String typeId) 
    {
        this.typeId = typeId;
    }

    public String getTypeId() 
    {
        return typeId;
    }
    public void setTypeName(String typeName) 
    {
        this.typeName = typeName;
    }

    public String getTypeName() 
    {
        return typeName;
    }
    public void setTypeExplain(String typeExplain) 
    {
        this.typeExplain = typeExplain;
    }

    public String getTypeExplain() 
    {
        return typeExplain;
    }
    public void setTypeSort(Integer typeSort) 
    {
        this.typeSort = typeSort;
    }

    public Integer getTypeSort() 
    {
        return typeSort;
    }
    public void setTypeShort(String typeShort) 
    {
        this.typeShort = typeShort;
    }

    public String getTypeShort() 
    {
        return typeShort;
    }
    public void setTypeBranchnum(String typeBranchnum) 
    {
        this.typeBranchnum = typeBranchnum;
    }

    public String getTypeBranchnum() 
    {
        return typeBranchnum;
    }
    public void setTypeRemark(String typeRemark) 
    {
        this.typeRemark = typeRemark;
    }

    public String getTypeRemark() 
    {
        return typeRemark;
    }
    public void setLastDate(String lastDate) 
    {
        this.lastDate = lastDate;
    }

    public String getLastDate() 
    {
        return lastDate;
    }
    public void setStateId(String stateId) 
    {
        this.stateId = stateId;
    }

    public String getStateId() 
    {
        return stateId;
    }
    public void setCreaterBy(String createrBy) 
    {
        this.createrBy = createrBy;
    }

    public String getCreaterBy() 
    {
        return createrBy;
    }
    public void setUpdaterName(String updaterName) 
    {
        this.updaterName = updaterName;
    }

    public String getUpdaterName() 
    {
        return updaterName;
    }
    public void setUpdaterTime(String updaterTime) 
    {
        this.updaterTime = updaterTime;
    }

    public String getUpdaterTime() 
    {
        return updaterTime;
    }
    public void setVersionNumber(String versionNumber) 
    {
        this.versionNumber = versionNumber;
    }

    public String getVersionNumber() 
    {
        return versionNumber;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }
    public void setConpanyCodee(String conpanyCodee) 
    {
        this.conpanyCodee = conpanyCodee;
    }

    public String getConpanyCodee() 
    {
        return conpanyCodee;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("typeId", getTypeId())
            .append("typeName", getTypeName())
            .append("typeExplain", getTypeExplain())
            .append("typeSort", getTypeSort())
            .append("typeShort", getTypeShort())
            .append("typeBranchnum", getTypeBranchnum())
            .append("typeRemark", getTypeRemark())
            .append("lastDate", getLastDate())
            .append("stateId", getStateId())
            .append("createrBy", getCreaterBy())
            .append("createTime", getCreateTime())
            .append("updaterName", getUpdaterName())
            .append("updaterTime", getUpdaterTime())
            .append("versionNumber", getVersionNumber())
            .append("remarks", getRemarks())
            .append("conpanyCodee", getConpanyCodee())
            .toString();
    }
}
