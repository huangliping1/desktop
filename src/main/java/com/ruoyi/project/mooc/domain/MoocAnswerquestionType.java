package com.ruoyi.project.mooc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 mooc_answerquestion_type
 * 
 * @author administrator
 * @date 2020-05-06
 */
public class MoocAnswerquestionType extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 分类编号 */
    private String classifyId;

    /** 分类关联号 */
    @Excel(name = "分类关联号")
    private String classifyRelated;

    /** 分类说明 */
    @Excel(name = "分类说明")
    private String classifyExpain;

    /** 分类名称 */
    @Excel(name = "分类名称")
    private String classifyName;

    /** 分类依据 */
    @Excel(name = "分类依据")
    private String classifyAccroding;

    /** 分类类别 */
    @Excel(name = "分类类别")
    private String classifyType;

    /** 所属大类 */
    @Excel(name = "所属大类")
    private String classifyMaintype;

    /** 分类描述 */
    @Excel(name = "分类描述")
    private String classifyDescibe;

    /** 分类编号 */
    @Excel(name = "分类编号")
    private String classifyNumber;

    /** 分类排序 */
    @Excel(name = "分类排序")
    private String classifySort;

    /** 分类状态 */
    @Excel(name = "分类状态")
    private String classifyState;

    /** 分类备注 */
    @Excel(name = "分类备注")
    private String classifyRemark;

    /** 最近使用日期 */
    @Excel(name = "最近使用日期")
    private String lastDate;

    /** 状态 */
    @Excel(name = "状态")
    private String stateId;

    /** 创建者 */
    @Excel(name = "创建者")
    private String createrBy;

    /** 更新人 */
    @Excel(name = "更新人")
    private String updaterName;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private String updaterTime;

    /** 版本号 */
    @Excel(name = "版本号")
    private String versionNumber;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    /** 公司机构名 */
    @Excel(name = "公司机构名")
    private String conpanyCodee;

    public void setClassifyId(String classifyId) 
    {
        this.classifyId = classifyId;
    }

    public String getClassifyId() 
    {
        return classifyId;
    }
    public void setClassifyRelated(String classifyRelated) 
    {
        this.classifyRelated = classifyRelated;
    }

    public String getClassifyRelated() 
    {
        return classifyRelated;
    }
    public void setClassifyExpain(String classifyExpain) 
    {
        this.classifyExpain = classifyExpain;
    }

    public String getClassifyExpain() 
    {
        return classifyExpain;
    }
    public void setClassifyName(String classifyName) 
    {
        this.classifyName = classifyName;
    }

    public String getClassifyName() 
    {
        return classifyName;
    }
    public void setClassifyAccroding(String classifyAccroding) 
    {
        this.classifyAccroding = classifyAccroding;
    }

    public String getClassifyAccroding() 
    {
        return classifyAccroding;
    }
    public void setClassifyType(String classifyType) 
    {
        this.classifyType = classifyType;
    }

    public String getClassifyType() 
    {
        return classifyType;
    }
    public void setClassifyMaintype(String classifyMaintype) 
    {
        this.classifyMaintype = classifyMaintype;
    }

    public String getClassifyMaintype() 
    {
        return classifyMaintype;
    }
    public void setClassifyDescibe(String classifyDescibe) 
    {
        this.classifyDescibe = classifyDescibe;
    }

    public String getClassifyDescibe() 
    {
        return classifyDescibe;
    }
    public void setClassifyNumber(String classifyNumber) 
    {
        this.classifyNumber = classifyNumber;
    }

    public String getClassifyNumber() 
    {
        return classifyNumber;
    }
    public void setClassifySort(String classifySort) 
    {
        this.classifySort = classifySort;
    }

    public String getClassifySort() 
    {
        return classifySort;
    }
    public void setClassifyState(String classifyState) 
    {
        this.classifyState = classifyState;
    }

    public String getClassifyState() 
    {
        return classifyState;
    }
    public void setClassifyRemark(String classifyRemark) 
    {
        this.classifyRemark = classifyRemark;
    }

    public String getClassifyRemark() 
    {
        return classifyRemark;
    }
    public void setLastDate(String lastDate) 
    {
        this.lastDate = lastDate;
    }

    public String getLastDate() 
    {
        return lastDate;
    }
    public void setStateId(String stateId) 
    {
        this.stateId = stateId;
    }

    public String getStateId() 
    {
        return stateId;
    }
    public void setCreaterBy(String createrBy) 
    {
        this.createrBy = createrBy;
    }

    public String getCreaterBy() 
    {
        return createrBy;
    }
    public void setUpdaterName(String updaterName) 
    {
        this.updaterName = updaterName;
    }

    public String getUpdaterName() 
    {
        return updaterName;
    }
    public void setUpdaterTime(String updaterTime) 
    {
        this.updaterTime = updaterTime;
    }

    public String getUpdaterTime() 
    {
        return updaterTime;
    }
    public void setVersionNumber(String versionNumber) 
    {
        this.versionNumber = versionNumber;
    }

    public String getVersionNumber() 
    {
        return versionNumber;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }
    public void setConpanyCodee(String conpanyCodee) 
    {
        this.conpanyCodee = conpanyCodee;
    }

    public String getConpanyCodee() 
    {
        return conpanyCodee;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("classifyId", getClassifyId())
            .append("classifyRelated", getClassifyRelated())
            .append("classifyExpain", getClassifyExpain())
            .append("classifyName", getClassifyName())
            .append("classifyAccroding", getClassifyAccroding())
            .append("classifyType", getClassifyType())
            .append("classifyMaintype", getClassifyMaintype())
            .append("classifyDescibe", getClassifyDescibe())
            .append("classifyNumber", getClassifyNumber())
            .append("classifySort", getClassifySort())
            .append("classifyState", getClassifyState())
            .append("classifyRemark", getClassifyRemark())
            .append("lastDate", getLastDate())
            .append("stateId", getStateId())
            .append("createrBy", getCreaterBy())
            .append("createTime", getCreateTime())
            .append("updaterName", getUpdaterName())
            .append("updaterTime", getUpdaterTime())
            .append("versionNumber", getVersionNumber())
            .append("remarks", getRemarks())
            .append("conpanyCodee", getConpanyCodee())
            .toString();
    }
}
