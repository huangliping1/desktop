package com.ruoyi.project.mooc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 mooc_commentary
 * 
 * @author administrator
 * @date 2020-05-06
 */
public class MoocCommentary extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 评论编号 */
    private String commentaryId;

    /** 评论课程 */
    @Excel(name = "评论课程")
    private String commentaryCourse;

    /** 评论内容 */
    @Excel(name = "评论内容")
    private String commentaryContent;

    /** 评论排序 */
    @Excel(name = "评论排序")
    private Integer commentarySort;

    /** 评论关联单号 */
    @Excel(name = "评论关联单号")
    private String commentaryRelate;

    /** 评论人 */
    @Excel(name = "评论人")
    private String commentaryPeople;

    /** 评论时间 */
    @Excel(name = "评论时间")
    private String commentaryTime;

    /** 评论类别 */
    @Excel(name = "评论类别")
    private String commentaryType;

    /** 评论附加内容 */
    @Excel(name = "评论附加内容")
    private String commentaryAdd;

    /** 评论次数 */
    @Excel(name = "评论次数")
    private String commentaryFrequency;

    /** 评论类型好差评 */
    @Excel(name = "评论类型好差评")
    private String commentaryMold;

    /** 评论备注 */
    @Excel(name = "评论备注")
    private String commentaryRemark;

    /** 最近使用日期 */
    @Excel(name = "最近使用日期")
    private String lastDate;

    /** 状态 */
    @Excel(name = "状态")
    private String stateId;

    /** 创建者 */
    @Excel(name = "创建者")
    private String createrBy;

    /** 更新人 */
    @Excel(name = "更新人")
    private String updaterName;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private String updaterTime;

    /** 版本号 */
    @Excel(name = "版本号")
    private String versionNumber;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    /** 公司机构名 */
    @Excel(name = "公司机构名")
    private String conpanyCodee;

    public void setCommentaryId(String commentaryId) 
    {
        this.commentaryId = commentaryId;
    }

    public String getCommentaryId() 
    {
        return commentaryId;
    }
    public void setCommentaryCourse(String commentaryCourse) 
    {
        this.commentaryCourse = commentaryCourse;
    }

    public String getCommentaryCourse() 
    {
        return commentaryCourse;
    }
    public void setCommentaryContent(String commentaryContent) 
    {
        this.commentaryContent = commentaryContent;
    }

    public String getCommentaryContent() 
    {
        return commentaryContent;
    }
    public void setCommentarySort(Integer commentarySort) 
    {
        this.commentarySort = commentarySort;
    }

    public Integer getCommentarySort() 
    {
        return commentarySort;
    }
    public void setCommentaryRelate(String commentaryRelate) 
    {
        this.commentaryRelate = commentaryRelate;
    }

    public String getCommentaryRelate() 
    {
        return commentaryRelate;
    }
    public void setCommentaryPeople(String commentaryPeople) 
    {
        this.commentaryPeople = commentaryPeople;
    }

    public String getCommentaryPeople() 
    {
        return commentaryPeople;
    }
    public void setCommentaryTime(String commentaryTime) 
    {
        this.commentaryTime = commentaryTime;
    }

    public String getCommentaryTime() 
    {
        return commentaryTime;
    }
    public void setCommentaryType(String commentaryType) 
    {
        this.commentaryType = commentaryType;
    }

    public String getCommentaryType() 
    {
        return commentaryType;
    }
    public void setCommentaryAdd(String commentaryAdd) 
    {
        this.commentaryAdd = commentaryAdd;
    }

    public String getCommentaryAdd() 
    {
        return commentaryAdd;
    }
    public void setCommentaryFrequency(String commentaryFrequency) 
    {
        this.commentaryFrequency = commentaryFrequency;
    }

    public String getCommentaryFrequency() 
    {
        return commentaryFrequency;
    }
    public void setCommentaryMold(String commentaryMold) 
    {
        this.commentaryMold = commentaryMold;
    }

    public String getCommentaryMold() 
    {
        return commentaryMold;
    }
    public void setCommentaryRemark(String commentaryRemark) 
    {
        this.commentaryRemark = commentaryRemark;
    }

    public String getCommentaryRemark() 
    {
        return commentaryRemark;
    }
    public void setLastDate(String lastDate) 
    {
        this.lastDate = lastDate;
    }

    public String getLastDate() 
    {
        return lastDate;
    }
    public void setStateId(String stateId) 
    {
        this.stateId = stateId;
    }

    public String getStateId() 
    {
        return stateId;
    }
    public void setCreaterBy(String createrBy) 
    {
        this.createrBy = createrBy;
    }

    public String getCreaterBy() 
    {
        return createrBy;
    }
    public void setUpdaterName(String updaterName) 
    {
        this.updaterName = updaterName;
    }

    public String getUpdaterName() 
    {
        return updaterName;
    }
    public void setUpdaterTime(String updaterTime) 
    {
        this.updaterTime = updaterTime;
    }

    public String getUpdaterTime() 
    {
        return updaterTime;
    }
    public void setVersionNumber(String versionNumber) 
    {
        this.versionNumber = versionNumber;
    }

    public String getVersionNumber() 
    {
        return versionNumber;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }
    public void setConpanyCodee(String conpanyCodee) 
    {
        this.conpanyCodee = conpanyCodee;
    }

    public String getConpanyCodee() 
    {
        return conpanyCodee;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("commentaryId", getCommentaryId())
            .append("commentaryCourse", getCommentaryCourse())
            .append("commentaryContent", getCommentaryContent())
            .append("commentarySort", getCommentarySort())
            .append("commentaryRelate", getCommentaryRelate())
            .append("commentaryPeople", getCommentaryPeople())
            .append("commentaryTime", getCommentaryTime())
            .append("commentaryType", getCommentaryType())
            .append("commentaryAdd", getCommentaryAdd())
            .append("commentaryFrequency", getCommentaryFrequency())
            .append("commentaryMold", getCommentaryMold())
            .append("commentaryRemark", getCommentaryRemark())
            .append("lastDate", getLastDate())
            .append("stateId", getStateId())
            .append("createrBy", getCreaterBy())
            .append("createTime", getCreateTime())
            .append("updaterName", getUpdaterName())
            .append("updaterTime", getUpdaterTime())
            .append("versionNumber", getVersionNumber())
            .append("remarks", getRemarks())
            .append("conpanyCodee", getConpanyCodee())
            .toString();
    }
}
