package com.ruoyi.project.mooc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 mooc_hotsort
 * 
 * @author administrator
 * @date 2020-05-06
 */
public class MoocHotsort extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 排行编号 */
    private String rankingId;

    /** 排行模块 */
    @Excel(name = "排行模块")
    private String rankingModule;

    /** 排行内容 */
    @Excel(name = "排行内容")
    private String rankingSubstance;

    /** 排行类别 */
    @Excel(name = "排行类别")
    private String rankingType;

    /** 排行位置 */
    @Excel(name = "排行位置")
    private String rankingPlace;

    /** 排行分类 */
    @Excel(name = "排行分类")
    private String rankingClassify;

    /** 排行名称 */
    @Excel(name = "排行名称")
    private String rankingName;

    /** 排行时间 */
    @Excel(name = "排行时间")
    private String rankingTime;

    /** 排行说明 */
    @Excel(name = "排行说明")
    private String rankingComtent;

    /** 排行大类 */
    @Excel(name = "排行大类")
    private String rankingHanding;

    /** 排行名次 */
    @Excel(name = "排行名次")
    private String rankingNumker;

    /** 排行人数 */
    @Excel(name = "排行人数")
    private String rankingPeoples;

    /** 排行排序 */
    @Excel(name = "排行排序")
    private String rankingSort;

    /** 最近使用日期 */
    @Excel(name = "最近使用日期")
    private String lastDate;

    /** 状态 */
    @Excel(name = "状态")
    private String stateId;

    /** 创建者 */
    @Excel(name = "创建者")
    private String createrBy;

    /** 更新人 */
    @Excel(name = "更新人")
    private String updaterName;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private String updaterTime;

    /** 版本号 */
    @Excel(name = "版本号")
    private String versionNumber;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    /** 公司机构名 */
    @Excel(name = "公司机构名")
    private String conpanyCodee;

    public void setRankingId(String rankingId) 
    {
        this.rankingId = rankingId;
    }

    public String getRankingId() 
    {
        return rankingId;
    }
    public void setRankingModule(String rankingModule) 
    {
        this.rankingModule = rankingModule;
    }

    public String getRankingModule() 
    {
        return rankingModule;
    }
    public void setRankingSubstance(String rankingSubstance) 
    {
        this.rankingSubstance = rankingSubstance;
    }

    public String getRankingSubstance() 
    {
        return rankingSubstance;
    }
    public void setRankingType(String rankingType) 
    {
        this.rankingType = rankingType;
    }

    public String getRankingType() 
    {
        return rankingType;
    }
    public void setRankingPlace(String rankingPlace) 
    {
        this.rankingPlace = rankingPlace;
    }

    public String getRankingPlace() 
    {
        return rankingPlace;
    }
    public void setRankingClassify(String rankingClassify) 
    {
        this.rankingClassify = rankingClassify;
    }

    public String getRankingClassify() 
    {
        return rankingClassify;
    }
    public void setRankingName(String rankingName) 
    {
        this.rankingName = rankingName;
    }

    public String getRankingName() 
    {
        return rankingName;
    }
    public void setRankingTime(String rankingTime) 
    {
        this.rankingTime = rankingTime;
    }

    public String getRankingTime() 
    {
        return rankingTime;
    }
    public void setRankingComtent(String rankingComtent) 
    {
        this.rankingComtent = rankingComtent;
    }

    public String getRankingComtent() 
    {
        return rankingComtent;
    }
    public void setRankingHanding(String rankingHanding) 
    {
        this.rankingHanding = rankingHanding;
    }

    public String getRankingHanding() 
    {
        return rankingHanding;
    }
    public void setRankingNumker(String rankingNumker) 
    {
        this.rankingNumker = rankingNumker;
    }

    public String getRankingNumker() 
    {
        return rankingNumker;
    }
    public void setRankingPeoples(String rankingPeoples) 
    {
        this.rankingPeoples = rankingPeoples;
    }

    public String getRankingPeoples() 
    {
        return rankingPeoples;
    }
    public void setRankingSort(String rankingSort) 
    {
        this.rankingSort = rankingSort;
    }

    public String getRankingSort() 
    {
        return rankingSort;
    }
    public void setLastDate(String lastDate) 
    {
        this.lastDate = lastDate;
    }

    public String getLastDate() 
    {
        return lastDate;
    }
    public void setStateId(String stateId) 
    {
        this.stateId = stateId;
    }

    public String getStateId() 
    {
        return stateId;
    }
    public void setCreaterBy(String createrBy) 
    {
        this.createrBy = createrBy;
    }

    public String getCreaterBy() 
    {
        return createrBy;
    }
    public void setUpdaterName(String updaterName) 
    {
        this.updaterName = updaterName;
    }

    public String getUpdaterName() 
    {
        return updaterName;
    }
    public void setUpdaterTime(String updaterTime) 
    {
        this.updaterTime = updaterTime;
    }

    public String getUpdaterTime() 
    {
        return updaterTime;
    }
    public void setVersionNumber(String versionNumber) 
    {
        this.versionNumber = versionNumber;
    }

    public String getVersionNumber() 
    {
        return versionNumber;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }
    public void setConpanyCodee(String conpanyCodee) 
    {
        this.conpanyCodee = conpanyCodee;
    }

    public String getConpanyCodee() 
    {
        return conpanyCodee;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("rankingId", getRankingId())
            .append("rankingModule", getRankingModule())
            .append("rankingSubstance", getRankingSubstance())
            .append("rankingType", getRankingType())
            .append("rankingPlace", getRankingPlace())
            .append("rankingClassify", getRankingClassify())
            .append("rankingName", getRankingName())
            .append("rankingTime", getRankingTime())
            .append("rankingComtent", getRankingComtent())
            .append("rankingHanding", getRankingHanding())
            .append("rankingNumker", getRankingNumker())
            .append("rankingPeoples", getRankingPeoples())
            .append("rankingSort", getRankingSort())
            .append("lastDate", getLastDate())
            .append("stateId", getStateId())
            .append("createrBy", getCreaterBy())
            .append("createTime", getCreateTime())
            .append("updaterName", getUpdaterName())
            .append("updaterTime", getUpdaterTime())
            .append("versionNumber", getVersionNumber())
            .append("remarks", getRemarks())
            .append("conpanyCodee", getConpanyCodee())
            .toString();
    }
}
