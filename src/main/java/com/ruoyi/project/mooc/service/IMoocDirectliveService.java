package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.project.mooc.domain.MoocDirectlive;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author administrator
 * @date 2020-05-06
 */
public interface IMoocDirectliveService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param videoId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public MoocDirectlive selectMoocDirectliveById(Long videoId);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocDirectlive 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MoocDirectlive> selectMoocDirectliveList(MoocDirectlive moocDirectlive);

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocDirectlive 【请填写功能名称】
     * @return 结果
     */
    public int insertMoocDirectlive(MoocDirectlive moocDirectlive);

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocDirectlive 【请填写功能名称】
     * @return 结果
     */
    public int updateMoocDirectlive(MoocDirectlive moocDirectlive);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMoocDirectliveByIds(String ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param videoId 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteMoocDirectliveById(Long videoId);
}
