package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mooc.mapper.MoocCourseMapper;
import com.ruoyi.project.mooc.domain.MoocCourse;
import com.ruoyi.project.mooc.service.IMoocCourseService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author administrator
 * @date 2020-05-06
 */
@Service
public class MoocCourseServiceImpl implements IMoocCourseService 
{
    @Autowired
    private MoocCourseMapper moocCourseMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param courseId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public MoocCourse selectMoocCourseById(String courseId)
    {
        return moocCourseMapper.selectMoocCourseById(courseId);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocCourse 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MoocCourse> selectMoocCourseList(MoocCourse moocCourse)
    {
        return moocCourseMapper.selectMoocCourseList(moocCourse);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocCourse 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMoocCourse(MoocCourse moocCourse)
    {
        moocCourse.setCreateTime(DateUtils.getNowDate());
        return moocCourseMapper.insertMoocCourse(moocCourse);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocCourse 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMoocCourse(MoocCourse moocCourse)
    {
        return moocCourseMapper.updateMoocCourse(moocCourse);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMoocCourseByIds(String ids)
    {
        return moocCourseMapper.deleteMoocCourseByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param courseId 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteMoocCourseById(String courseId)
    {
        return moocCourseMapper.deleteMoocCourseById(courseId);
    }
}
