package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.project.mooc.domain.MoocQuestion;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author administrator
 * @date 2020-05-06
 */
public interface IMoocQuestionService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param questionId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public MoocQuestion selectMoocQuestionById(String questionId);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocQuestion 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MoocQuestion> selectMoocQuestionList(MoocQuestion moocQuestion);

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocQuestion 【请填写功能名称】
     * @return 结果
     */
    public int insertMoocQuestion(MoocQuestion moocQuestion);

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocQuestion 【请填写功能名称】
     * @return 结果
     */
    public int updateMoocQuestion(MoocQuestion moocQuestion);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMoocQuestionByIds(String ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param questionId 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteMoocQuestionById(String questionId);
}
