package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mooc.mapper.MoocDirectliveMapper;
import com.ruoyi.project.mooc.domain.MoocDirectlive;
import com.ruoyi.project.mooc.service.IMoocDirectliveService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author administrator
 * @date 2020-05-06
 */
@Service
public class MoocDirectliveServiceImpl implements IMoocDirectliveService 
{
    @Autowired
    private MoocDirectliveMapper moocDirectliveMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param videoId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public MoocDirectlive selectMoocDirectliveById(Long videoId)
    {
        return moocDirectliveMapper.selectMoocDirectliveById(videoId);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocDirectlive 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MoocDirectlive> selectMoocDirectliveList(MoocDirectlive moocDirectlive)
    {
        return moocDirectliveMapper.selectMoocDirectliveList(moocDirectlive);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocDirectlive 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMoocDirectlive(MoocDirectlive moocDirectlive)
    {
        moocDirectlive.setCreateTime(DateUtils.getNowDate());
        return moocDirectliveMapper.insertMoocDirectlive(moocDirectlive);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocDirectlive 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMoocDirectlive(MoocDirectlive moocDirectlive)
    {
        moocDirectlive.setUpdateTime(DateUtils.getNowDate());
        return moocDirectliveMapper.updateMoocDirectlive(moocDirectlive);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMoocDirectliveByIds(String ids)
    {
        return moocDirectliveMapper.deleteMoocDirectliveByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param videoId 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteMoocDirectliveById(Long videoId)
    {
        return moocDirectliveMapper.deleteMoocDirectliveById(videoId);
    }
}
