package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.project.mooc.domain.MoocCourseList;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author administrator
 * @date 2020-05-06
 */
public interface IMoocCourseListService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param typeId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public MoocCourseList selectMoocCourseListById(String typeId);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocCourseList 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MoocCourseList> selectMoocCourseListList(MoocCourseList moocCourseList);

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocCourseList 【请填写功能名称】
     * @return 结果
     */
    public int insertMoocCourseList(MoocCourseList moocCourseList);

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocCourseList 【请填写功能名称】
     * @return 结果
     */
    public int updateMoocCourseList(MoocCourseList moocCourseList);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMoocCourseListByIds(String ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param typeId 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteMoocCourseListById(String typeId);
}
