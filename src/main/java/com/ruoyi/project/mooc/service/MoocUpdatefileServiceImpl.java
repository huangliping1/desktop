package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mooc.mapper.MoocUpdatefileMapper;
import com.ruoyi.project.mooc.domain.MoocUpdatefile;
import com.ruoyi.project.mooc.service.IMoocUpdatefileService;
import com.ruoyi.common.utils.text.Convert;
import com.ruoyi.framework.web.controller.BaseController;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author administrator
 * @date 2020-05-13
 */
@Service
public class MoocUpdatefileServiceImpl extends BaseController implements IMoocUpdatefileService 
{
    @Autowired
    private MoocUpdatefileMapper moocUpdatefileMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param idName 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public MoocUpdatefile selectMoocUpdatefileById(String idName)
    {
        return moocUpdatefileMapper.selectMoocUpdatefileById(idName);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocUpdatefile 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MoocUpdatefile> selectMoocUpdatefileList(MoocUpdatefile moocUpdatefile)
    {
        return moocUpdatefileMapper.selectMoocUpdatefileList(moocUpdatefile);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocUpdatefile 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMoocUpdatefile(MoocUpdatefile moocUpdatefile)
    {
        moocUpdatefile.setCreateTime(DateUtils.getNowDate());
        return moocUpdatefileMapper.insertMoocUpdatefile(moocUpdatefile);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocUpdatefile 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMoocUpdatefile(MoocUpdatefile moocUpdatefile)
    {
        return moocUpdatefileMapper.updateMoocUpdatefile(moocUpdatefile);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMoocUpdatefileByIds(String ids)
    {
        return moocUpdatefileMapper.deleteMoocUpdatefileByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idName 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteMoocUpdatefileById(String idName)
    {
        return moocUpdatefileMapper.deleteMoocUpdatefileById(idName);
    }
}
