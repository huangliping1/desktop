package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.project.mooc.domain.MoocCourseContent;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author administrator
 * @date 2020-05-06
 */
public interface IMoocCourseContentService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param typeId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public MoocCourseContent selectMoocCourseContentById(String typeId);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocCourseContent 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MoocCourseContent> selectMoocCourseContentList(MoocCourseContent moocCourseContent);

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocCourseContent 【请填写功能名称】
     * @return 结果
     */
    public int insertMoocCourseContent(MoocCourseContent moocCourseContent);

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocCourseContent 【请填写功能名称】
     * @return 结果
     */
    public int updateMoocCourseContent(MoocCourseContent moocCourseContent);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMoocCourseContentByIds(String ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param typeId 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteMoocCourseContentById(String typeId);
}
