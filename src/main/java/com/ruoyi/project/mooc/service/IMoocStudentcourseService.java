package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.project.mooc.domain.MoocStudentcourse;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author administrator
 * @date 2020-05-06
 */
public interface IMoocStudentcourseService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param coursetableId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public MoocStudentcourse selectMoocStudentcourseById(String coursetableId);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocStudentcourse 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MoocStudentcourse> selectMoocStudentcourseList(MoocStudentcourse moocStudentcourse);

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocStudentcourse 【请填写功能名称】
     * @return 结果
     */
    public int insertMoocStudentcourse(MoocStudentcourse moocStudentcourse);

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocStudentcourse 【请填写功能名称】
     * @return 结果
     */
    public int updateMoocStudentcourse(MoocStudentcourse moocStudentcourse);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMoocStudentcourseByIds(String ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param coursetableId 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteMoocStudentcourseById(String coursetableId);
}
