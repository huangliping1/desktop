package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mooc.mapper.MoocPicwordMapper;
import com.ruoyi.project.mooc.domain.MoocPicword;
import com.ruoyi.project.mooc.service.IMoocPicwordService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author administrator
 * @date 2020-05-13
 */
@Service
public class MoocPicwordServiceImpl implements IMoocPicwordService 
{
    @Autowired
    private MoocPicwordMapper moocPicwordMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param idName 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public MoocPicword selectMoocPicwordById(String idName)
    {
        return moocPicwordMapper.selectMoocPicwordById(idName);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocPicword 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MoocPicword> selectMoocPicwordList(MoocPicword moocPicword)
    {
        return moocPicwordMapper.selectMoocPicwordList(moocPicword);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocPicword 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMoocPicword(MoocPicword moocPicword)
    {
        moocPicword.setCreateTime(DateUtils.getNowDate());
        return moocPicwordMapper.insertMoocPicword(moocPicword);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocPicword 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMoocPicword(MoocPicword moocPicword)
    {
        return moocPicwordMapper.updateMoocPicword(moocPicword);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMoocPicwordByIds(String ids)
    {
        return moocPicwordMapper.deleteMoocPicwordByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idName 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteMoocPicwordById(String idName)
    {
        return moocPicwordMapper.deleteMoocPicwordById(idName);
    }
}
