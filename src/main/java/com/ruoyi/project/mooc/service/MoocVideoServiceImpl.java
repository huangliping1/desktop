package com.ruoyi.project.mooc.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.security.ShiroUtils;
import com.ruoyi.common.utils.text.Convert;
import com.ruoyi.project.mooc.domain.MoocVideo;
import com.ruoyi.project.mooc.domain.MoocVideop;
import com.ruoyi.project.mooc.mapper.MoocVideoMapper;
import com.ruoyi.project.system.dict.domain.DictType;
import com.ruoyi.project.system.dict.mapper.DictDataMapper;
import com.ruoyi.project.system.dict.mapper.DictTypeMapper;
import com.ruoyi.project.system.user.domain.User;

/**
 * 字典 业务层处理
 * 
 * @author ruoyi
 */
@Service
public class MoocVideoServiceImpl implements MoocVideoService  
{
	
	
	  @Autowired
	    private MoocVideoMapper moocVideoMapper;

	    /**
	     * 查询视频信息
	     * 
	     * @param videoId 视频信息ID
	     * @return 视频信息
	     */
	    @Override
	    public MoocVideo selectMoocVideoById(Long videoId)
	    {
	        return moocVideoMapper.selectMoocVideoById(videoId);
	    }

	    /**
	     * 查询视频信息列表
	     * 
	     * @param moocVideo 视频信息
	     * @return 视频信息
	     */
	    @Override
	    public List<MoocVideo> selectMoocVideoList(MoocVideo moocVideo)
	    {
	        return moocVideoMapper.selectMoocVideoList(moocVideo);
	    }

	    @Override
	    public List<MoocVideop> selectMoocVideoListp(MoocVideop moocVideop)
	    {
	        return moocVideoMapper.selectMoocVideoListp(moocVideop);
	    }
	    
//		@Autowired
//		DateUtils dateutils;
	    /**
	     * 新增视频信息
	     * 
	     * @param moocVideo 视频信息
	     * @return 结果
	     */
	    @Override
	    public int insertMoocVideo(MoocVideo moocVideo)
	    {
	        moocVideo.setCreateTime(DateUtils.getNowDate());
	        long sz=12;

	        DateUtils.test("1");
	        Map hmap=new HashMap();
	        String  name = DateUtils.encodingkeyid(Long.toString(sz));
	//        String  id=UUID.randomUUID().toString().replace("-", "");
	 //       moocVideo.setVideoId(Long.parseLong(id));
	        moocVideo.setVodeoIdkey(name);
	        moocVideo.setVideoIdkey( name);
	        
            User user = ShiroUtils.getSysUser();
            String loginName = user.getLoginName();
            moocVideo.setCreateBy(loginName);
	        return moocVideoMapper.insertMoocVideo(moocVideo);
	    }

	    /**
	     * 修改视频信息
	     * 
	     * @param moocVideo 视频信息
	     * @return 结果
	     */
	    @Override
	    public int updateMoocVideo(MoocVideo moocVideo)
	    {
	        moocVideo.setUpdateTime(DateUtils.getNowDate());
	        return moocVideoMapper.updateMoocVideo(moocVideo);
	    }

	    /**
	     * 删除视频信息对象
	     * 
	     * @param ids 需要删除的数据ID
	     * @return 结果
	     */
	    @Override
	    public int deleteMoocVideoByIds(String ids)
	    {
	        return moocVideoMapper.deleteMoocVideoByIds(Convert.toStrArray(ids));
	    }

	    /**
	     * 删除视频信息信息
	     * 
	     * @param videoId 视频信息ID
	     * @return 结果
	     */
	    @Override
	    public int deleteMoocVideoById(Long videoId)
	    {
	        return moocVideoMapper.deleteMoocVideoById(videoId);
	    }
}
