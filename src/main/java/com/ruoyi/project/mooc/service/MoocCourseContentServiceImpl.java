package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mooc.mapper.MoocCourseContentMapper;
import com.ruoyi.project.mooc.domain.MoocCourseContent;
import com.ruoyi.project.mooc.service.IMoocCourseContentService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author administrator
 * @date 2020-05-06
 */
@Service
public class MoocCourseContentServiceImpl implements IMoocCourseContentService 
{
    @Autowired
    private MoocCourseContentMapper moocCourseContentMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param typeId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public MoocCourseContent selectMoocCourseContentById(String typeId)
    {
        return moocCourseContentMapper.selectMoocCourseContentById(typeId);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocCourseContent 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MoocCourseContent> selectMoocCourseContentList(MoocCourseContent moocCourseContent)
    {
        return moocCourseContentMapper.selectMoocCourseContentList(moocCourseContent);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocCourseContent 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMoocCourseContent(MoocCourseContent moocCourseContent)
    {
        moocCourseContent.setCreateTime(DateUtils.getNowDate());
        return moocCourseContentMapper.insertMoocCourseContent(moocCourseContent);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocCourseContent 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMoocCourseContent(MoocCourseContent moocCourseContent)
    {
        return moocCourseContentMapper.updateMoocCourseContent(moocCourseContent);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMoocCourseContentByIds(String ids)
    {
        return moocCourseContentMapper.deleteMoocCourseContentByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param typeId 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteMoocCourseContentById(String typeId)
    {
        return moocCourseContentMapper.deleteMoocCourseContentById(typeId);
    }
}
