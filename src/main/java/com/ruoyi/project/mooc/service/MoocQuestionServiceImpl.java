package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mooc.mapper.MoocQuestionMapper;
import com.ruoyi.project.mooc.domain.MoocQuestion;
import com.ruoyi.project.mooc.service.IMoocQuestionService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author administrator
 * @date 2020-05-06
 */
@Service
public class MoocQuestionServiceImpl implements IMoocQuestionService 
{
    @Autowired
    private MoocQuestionMapper moocQuestionMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param questionId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public MoocQuestion selectMoocQuestionById(String questionId)
    {
        return moocQuestionMapper.selectMoocQuestionById(questionId);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocQuestion 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MoocQuestion> selectMoocQuestionList(MoocQuestion moocQuestion)
    {
        return moocQuestionMapper.selectMoocQuestionList(moocQuestion);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocQuestion 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMoocQuestion(MoocQuestion moocQuestion)
    {
        moocQuestion.setCreateTime(DateUtils.getNowDate());
        return moocQuestionMapper.insertMoocQuestion(moocQuestion);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocQuestion 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMoocQuestion(MoocQuestion moocQuestion)
    {
        return moocQuestionMapper.updateMoocQuestion(moocQuestion);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMoocQuestionByIds(String ids)
    {
        return moocQuestionMapper.deleteMoocQuestionByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param questionId 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteMoocQuestionById(String questionId)
    {
        return moocQuestionMapper.deleteMoocQuestionById(questionId);
    }
}
