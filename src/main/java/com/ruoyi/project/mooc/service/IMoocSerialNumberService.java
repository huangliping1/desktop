package com.ruoyi.project.mooc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.project.mooc.domain.MoocSerialNumber;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author administrator
 * @date 2020-04-26
 */
public interface IMoocSerialNumberService 
{
	
    /**
     * 查询【请填写功能名称】
     * 
     * @param idName 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public MoocSerialNumber selectMoocSerialNumberById(String idName);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocSerialNumber 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MoocSerialNumber> selectMoocSerialNumberList(MoocSerialNumber moocSerialNumber);

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocSerialNumber 【请填写功能名称】
     * @return 结果
     */
    public int insertMoocSerialNumber(MoocSerialNumber moocSerialNumber);

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocSerialNumber 【请填写功能名称】
     * @return 结果
     */
    public int updateMoocSerialNumber(MoocSerialNumber moocSerialNumber);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMoocSerialNumberByIds(String ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idName 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteMoocSerialNumberById(String idName);
}
