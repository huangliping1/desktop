package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.project.mooc.domain.MoocAnswer;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author administrator
 * @date 2020-05-06
 */
public interface IMoocAnswerService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param answerId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public MoocAnswer selectMoocAnswerById(String answerId);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocAnswer 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MoocAnswer> selectMoocAnswerList(MoocAnswer moocAnswer);

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocAnswer 【请填写功能名称】
     * @return 结果
     */
    public int insertMoocAnswer(MoocAnswer moocAnswer);

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocAnswer 【请填写功能名称】
     * @return 结果
     */
    public int updateMoocAnswer(MoocAnswer moocAnswer);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMoocAnswerByIds(String ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param answerId 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteMoocAnswerById(String answerId);
}
