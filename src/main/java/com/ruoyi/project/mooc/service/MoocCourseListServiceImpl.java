package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mooc.mapper.MoocCourseListMapper;
import com.ruoyi.project.mooc.domain.MoocCourseList;
import com.ruoyi.project.mooc.service.IMoocCourseListService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author administrator
 * @date 2020-05-06
 */
@Service
public class MoocCourseListServiceImpl implements IMoocCourseListService 
{
    @Autowired
    private MoocCourseListMapper moocCourseListMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param typeId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public MoocCourseList selectMoocCourseListById(String typeId)
    {
        return moocCourseListMapper.selectMoocCourseListById(typeId);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocCourseList 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MoocCourseList> selectMoocCourseListList(MoocCourseList moocCourseList)
    {
        return moocCourseListMapper.selectMoocCourseListList(moocCourseList);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocCourseList 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMoocCourseList(MoocCourseList moocCourseList)
    {
        moocCourseList.setCreateTime(DateUtils.getNowDate());
        return moocCourseListMapper.insertMoocCourseList(moocCourseList);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocCourseList 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMoocCourseList(MoocCourseList moocCourseList)
    {
        return moocCourseListMapper.updateMoocCourseList(moocCourseList);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMoocCourseListByIds(String ids)
    {
        return moocCourseListMapper.deleteMoocCourseListByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param typeId 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteMoocCourseListById(String typeId)
    {
        return moocCourseListMapper.deleteMoocCourseListById(typeId);
    }
}
