package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mooc.mapper.MoocCommentaryMapper;
import com.ruoyi.project.mooc.domain.MoocCommentary;
import com.ruoyi.project.mooc.service.IMoocCommentaryService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author administrator
 * @date 2020-05-06
 */
@Service
public class MoocCommentaryServiceImpl implements IMoocCommentaryService 
{
    @Autowired
    private MoocCommentaryMapper moocCommentaryMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param commentaryId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public MoocCommentary selectMoocCommentaryById(String commentaryId)
    {
        return moocCommentaryMapper.selectMoocCommentaryById(commentaryId);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocCommentary 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MoocCommentary> selectMoocCommentaryList(MoocCommentary moocCommentary)
    {
        return moocCommentaryMapper.selectMoocCommentaryList(moocCommentary);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocCommentary 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMoocCommentary(MoocCommentary moocCommentary)
    {
        moocCommentary.setCreateTime(DateUtils.getNowDate());
        return moocCommentaryMapper.insertMoocCommentary(moocCommentary);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocCommentary 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMoocCommentary(MoocCommentary moocCommentary)
    {
        return moocCommentaryMapper.updateMoocCommentary(moocCommentary);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMoocCommentaryByIds(String ids)
    {
        return moocCommentaryMapper.deleteMoocCommentaryByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param commentaryId 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteMoocCommentaryById(String commentaryId)
    {
        return moocCommentaryMapper.deleteMoocCommentaryById(commentaryId);
    }
}
