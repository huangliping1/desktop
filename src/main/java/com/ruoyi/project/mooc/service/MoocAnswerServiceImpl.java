package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mooc.mapper.MoocAnswerMapper;
import com.ruoyi.project.mooc.domain.MoocAnswer;
import com.ruoyi.project.mooc.service.IMoocAnswerService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author administrator
 * @date 2020-05-06
 */
@Service
public class MoocAnswerServiceImpl implements IMoocAnswerService 
{
    @Autowired
    private MoocAnswerMapper moocAnswerMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param answerId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public MoocAnswer selectMoocAnswerById(String answerId)
    {
        return moocAnswerMapper.selectMoocAnswerById(answerId);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocAnswer 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MoocAnswer> selectMoocAnswerList(MoocAnswer moocAnswer)
    {
        return moocAnswerMapper.selectMoocAnswerList(moocAnswer);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocAnswer 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMoocAnswer(MoocAnswer moocAnswer)
    {
        moocAnswer.setCreateTime(DateUtils.getNowDate());
        return moocAnswerMapper.insertMoocAnswer(moocAnswer);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocAnswer 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMoocAnswer(MoocAnswer moocAnswer)
    {
        return moocAnswerMapper.updateMoocAnswer(moocAnswer);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMoocAnswerByIds(String ids)
    {
        return moocAnswerMapper.deleteMoocAnswerByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param answerId 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteMoocAnswerById(String answerId)
    {
        return moocAnswerMapper.deleteMoocAnswerById(answerId);
    }
}
