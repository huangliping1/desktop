package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.project.mooc.domain.MoocUpdateSystem;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author administrator
 * @date 2020-04-26
 */
public interface IMoocUpdateSystemService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param uuid 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public MoocUpdateSystem selectMoocUpdateSystemById(String uuid);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocUpdateSystem 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MoocUpdateSystem> selectMoocUpdateSystemList(MoocUpdateSystem moocUpdateSystem);

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocUpdateSystem 【请填写功能名称】
     * @return 结果
     */
    public int insertMoocUpdateSystem(MoocUpdateSystem moocUpdateSystem);

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocUpdateSystem 【请填写功能名称】
     * @return 结果
     */
    public int updateMoocUpdateSystem(MoocUpdateSystem moocUpdateSystem);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMoocUpdateSystemByIds(String ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param uuid 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteMoocUpdateSystemById(String uuid);
}
