package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.project.mooc.domain.MoocCourse;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author administrator
 * @date 2020-05-06
 */
public interface IMoocCourseService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param courseId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public MoocCourse selectMoocCourseById(String courseId);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocCourse 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MoocCourse> selectMoocCourseList(MoocCourse moocCourse);

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocCourse 【请填写功能名称】
     * @return 结果
     */
    public int insertMoocCourse(MoocCourse moocCourse);

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocCourse 【请填写功能名称】
     * @return 结果
     */
    public int updateMoocCourse(MoocCourse moocCourse);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMoocCourseByIds(String ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param courseId 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteMoocCourseById(String courseId);
}
