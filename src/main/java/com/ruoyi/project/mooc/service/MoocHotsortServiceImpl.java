package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mooc.mapper.MoocHotsortMapper;
import com.ruoyi.project.mooc.domain.MoocHotsort;
import com.ruoyi.project.mooc.service.IMoocHotsortService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author administrator
 * @date 2020-05-06
 */
@Service
public class MoocHotsortServiceImpl implements IMoocHotsortService 
{
    @Autowired
    private MoocHotsortMapper moocHotsortMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param rankingId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public MoocHotsort selectMoocHotsortById(String rankingId)
    {
        return moocHotsortMapper.selectMoocHotsortById(rankingId);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocHotsort 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MoocHotsort> selectMoocHotsortList(MoocHotsort moocHotsort)
    {
        return moocHotsortMapper.selectMoocHotsortList(moocHotsort);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocHotsort 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMoocHotsort(MoocHotsort moocHotsort)
    {
        moocHotsort.setCreateTime(DateUtils.getNowDate());
        return moocHotsortMapper.insertMoocHotsort(moocHotsort);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocHotsort 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMoocHotsort(MoocHotsort moocHotsort)
    {
        return moocHotsortMapper.updateMoocHotsort(moocHotsort);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMoocHotsortByIds(String ids)
    {
        return moocHotsortMapper.deleteMoocHotsortByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param rankingId 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteMoocHotsortById(String rankingId)
    {
        return moocHotsortMapper.deleteMoocHotsortById(rankingId);
    }
}
