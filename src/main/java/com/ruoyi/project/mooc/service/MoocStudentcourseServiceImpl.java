package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mooc.mapper.MoocStudentcourseMapper;
import com.ruoyi.project.mooc.domain.MoocStudentcourse;
import com.ruoyi.project.mooc.service.IMoocStudentcourseService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author administrator
 * @date 2020-05-06
 */
@Service
public class MoocStudentcourseServiceImpl implements IMoocStudentcourseService 
{
    @Autowired
    private MoocStudentcourseMapper moocStudentcourseMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param coursetableId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public MoocStudentcourse selectMoocStudentcourseById(String coursetableId)
    {
        return moocStudentcourseMapper.selectMoocStudentcourseById(coursetableId);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocStudentcourse 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MoocStudentcourse> selectMoocStudentcourseList(MoocStudentcourse moocStudentcourse)
    {
        return moocStudentcourseMapper.selectMoocStudentcourseList(moocStudentcourse);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocStudentcourse 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMoocStudentcourse(MoocStudentcourse moocStudentcourse)
    {
        moocStudentcourse.setCreateTime(DateUtils.getNowDate());
        return moocStudentcourseMapper.insertMoocStudentcourse(moocStudentcourse);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocStudentcourse 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMoocStudentcourse(MoocStudentcourse moocStudentcourse)
    {
        return moocStudentcourseMapper.updateMoocStudentcourse(moocStudentcourse);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMoocStudentcourseByIds(String ids)
    {
        return moocStudentcourseMapper.deleteMoocStudentcourseByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param coursetableId 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteMoocStudentcourseById(String coursetableId)
    {
        return moocStudentcourseMapper.deleteMoocStudentcourseById(coursetableId);
    }
}
