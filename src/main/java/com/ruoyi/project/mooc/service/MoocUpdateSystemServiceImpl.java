package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mooc.mapper.MoocUpdateSystemMapper;
import com.ruoyi.project.mooc.domain.MoocUpdateSystem;
import com.ruoyi.project.mooc.service.IMoocUpdateSystemService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author administrator
 * @date 2020-04-26
 */
@Service
public class MoocUpdateSystemServiceImpl implements IMoocUpdateSystemService 
{
    @Autowired
    private MoocUpdateSystemMapper moocUpdateSystemMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param uuid 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public MoocUpdateSystem selectMoocUpdateSystemById(String uuid)
    {
        return moocUpdateSystemMapper.selectMoocUpdateSystemById(uuid);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocUpdateSystem 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MoocUpdateSystem> selectMoocUpdateSystemList(MoocUpdateSystem moocUpdateSystem)
    {
        return moocUpdateSystemMapper.selectMoocUpdateSystemList(moocUpdateSystem);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocUpdateSystem 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMoocUpdateSystem(MoocUpdateSystem moocUpdateSystem)
    {
        moocUpdateSystem.setCreateTime(DateUtils.getNowDate());
        long id=1;
        moocUpdateSystem.setUuid(DateUtils.encodingkeyid(Long.toString(id)));
        return moocUpdateSystemMapper.insertMoocUpdateSystem(moocUpdateSystem);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocUpdateSystem 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMoocUpdateSystem(MoocUpdateSystem moocUpdateSystem)
    {
        return moocUpdateSystemMapper.updateMoocUpdateSystem(moocUpdateSystem);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMoocUpdateSystemByIds(String ids)
    {
        return moocUpdateSystemMapper.deleteMoocUpdateSystemByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param uuid 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteMoocUpdateSystemById(String uuid)
    {
        return moocUpdateSystemMapper.deleteMoocUpdateSystemById(uuid);
    }
}
