package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mooc.mapper.MoocAnswerquestionTypeMapper;
import com.ruoyi.project.mooc.domain.MoocAnswerquestionType;
import com.ruoyi.project.mooc.service.IMoocAnswerquestionTypeService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author administrator
 * @date 2020-05-06
 */
@Service
public class MoocAnswerquestionTypeServiceImpl implements IMoocAnswerquestionTypeService 
{
    @Autowired
    private MoocAnswerquestionTypeMapper moocAnswerquestionTypeMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param classifyId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public MoocAnswerquestionType selectMoocAnswerquestionTypeById(String classifyId)
    {
        return moocAnswerquestionTypeMapper.selectMoocAnswerquestionTypeById(classifyId);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocAnswerquestionType 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MoocAnswerquestionType> selectMoocAnswerquestionTypeList(MoocAnswerquestionType moocAnswerquestionType)
    {
        return moocAnswerquestionTypeMapper.selectMoocAnswerquestionTypeList(moocAnswerquestionType);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocAnswerquestionType 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMoocAnswerquestionType(MoocAnswerquestionType moocAnswerquestionType)
    {
        moocAnswerquestionType.setCreateTime(DateUtils.getNowDate());
        return moocAnswerquestionTypeMapper.insertMoocAnswerquestionType(moocAnswerquestionType);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocAnswerquestionType 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMoocAnswerquestionType(MoocAnswerquestionType moocAnswerquestionType)
    {
        return moocAnswerquestionTypeMapper.updateMoocAnswerquestionType(moocAnswerquestionType);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMoocAnswerquestionTypeByIds(String ids)
    {
        return moocAnswerquestionTypeMapper.deleteMoocAnswerquestionTypeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param classifyId 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteMoocAnswerquestionTypeById(String classifyId)
    {
        return moocAnswerquestionTypeMapper.deleteMoocAnswerquestionTypeById(classifyId);
    }
}
