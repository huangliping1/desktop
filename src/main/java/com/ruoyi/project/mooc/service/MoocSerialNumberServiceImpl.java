package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mooc.mapper.MoocSerialNumberMapper;
import com.ruoyi.project.mooc.domain.MoocSerialNumber;
import com.ruoyi.project.mooc.service.IMoocSerialNumberService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author administrator
 * @date 2020-04-26
 */
@Service
public class MoocSerialNumberServiceImpl implements IMoocSerialNumberService 
{
    @Autowired
    private MoocSerialNumberMapper moocSerialNumberMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param idName 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public MoocSerialNumber selectMoocSerialNumberById(String idName)
    {
        return moocSerialNumberMapper.selectMoocSerialNumberById(idName);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocSerialNumber 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MoocSerialNumber> selectMoocSerialNumberList(MoocSerialNumber moocSerialNumber)
    {
        return moocSerialNumberMapper.selectMoocSerialNumberList(moocSerialNumber);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocSerialNumber 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMoocSerialNumber(MoocSerialNumber moocSerialNumber)
    {
        moocSerialNumber.setCreateTime(DateUtils.getNowDate());
   //     String name_id= "";
        long id=1;
        moocSerialNumber.setIdName(DateUtils.encodingkeyid(Long.toString(id)));
        return moocSerialNumberMapper.insertMoocSerialNumber(moocSerialNumber);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocSerialNumber 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMoocSerialNumber(MoocSerialNumber moocSerialNumber)
    {
    	
    	 moocSerialNumber.setCreateTime(DateUtils.getNowDate());
        return moocSerialNumberMapper.updateMoocSerialNumber(moocSerialNumber);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMoocSerialNumberByIds(String ids)
    {
        return moocSerialNumberMapper.deleteMoocSerialNumberByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idName 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteMoocSerialNumberById(String idName)
    {
        return moocSerialNumberMapper.deleteMoocSerialNumberById(idName);
    }
}
