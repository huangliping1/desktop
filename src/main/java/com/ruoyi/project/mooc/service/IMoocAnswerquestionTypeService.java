package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.project.mooc.domain.MoocAnswerquestionType;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author administrator
 * @date 2020-05-06
 */
public interface IMoocAnswerquestionTypeService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param classifyId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public MoocAnswerquestionType selectMoocAnswerquestionTypeById(String classifyId);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocAnswerquestionType 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MoocAnswerquestionType> selectMoocAnswerquestionTypeList(MoocAnswerquestionType moocAnswerquestionType);

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocAnswerquestionType 【请填写功能名称】
     * @return 结果
     */
    public int insertMoocAnswerquestionType(MoocAnswerquestionType moocAnswerquestionType);

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocAnswerquestionType 【请填写功能名称】
     * @return 结果
     */
    public int updateMoocAnswerquestionType(MoocAnswerquestionType moocAnswerquestionType);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMoocAnswerquestionTypeByIds(String ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param classifyId 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteMoocAnswerquestionTypeById(String classifyId);
}
