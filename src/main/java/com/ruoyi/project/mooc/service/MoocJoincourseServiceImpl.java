package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mooc.mapper.MoocJoincourseMapper;
import com.ruoyi.project.mooc.domain.MoocJoincourse;
import com.ruoyi.project.mooc.service.IMoocJoincourseService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author administrator
 * @date 2020-05-13
 */
@Service
public class MoocJoincourseServiceImpl implements IMoocJoincourseService 
{
    @Autowired
    private MoocJoincourseMapper moocJoincourseMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param joinId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public MoocJoincourse selectMoocJoincourseById(String joinId)
    {
        return moocJoincourseMapper.selectMoocJoincourseById(joinId);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocJoincourse 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MoocJoincourse> selectMoocJoincourseList(MoocJoincourse moocJoincourse)
    {
        return moocJoincourseMapper.selectMoocJoincourseList(moocJoincourse);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocJoincourse 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMoocJoincourse(MoocJoincourse moocJoincourse)
    {
        moocJoincourse.setCreateTime(DateUtils.getNowDate());
        return moocJoincourseMapper.insertMoocJoincourse(moocJoincourse);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocJoincourse 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMoocJoincourse(MoocJoincourse moocJoincourse)
    {
        return moocJoincourseMapper.updateMoocJoincourse(moocJoincourse);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMoocJoincourseByIds(String ids)
    {
        return moocJoincourseMapper.deleteMoocJoincourseByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param joinId 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteMoocJoincourseById(String joinId)
    {
        return moocJoincourseMapper.deleteMoocJoincourseById(joinId);
    }
}
