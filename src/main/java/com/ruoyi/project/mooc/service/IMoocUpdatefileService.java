package com.ruoyi.project.mooc.service;

import java.util.List;
import com.ruoyi.project.mooc.domain.MoocUpdatefile;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author administrator
 * @date 2020-05-13
 */
public interface IMoocUpdatefileService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param idName 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public MoocUpdatefile selectMoocUpdatefileById(String idName);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocUpdatefile 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MoocUpdatefile> selectMoocUpdatefileList(MoocUpdatefile moocUpdatefile);

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocUpdatefile 【请填写功能名称】
     * @return 结果
     */
    public int insertMoocUpdatefile(MoocUpdatefile moocUpdatefile);

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocUpdatefile 【请填写功能名称】
     * @return 结果
     */
    public int updateMoocUpdatefile(MoocUpdatefile moocUpdatefile);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMoocUpdatefileByIds(String ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idName 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteMoocUpdatefileById(String idName);
}
