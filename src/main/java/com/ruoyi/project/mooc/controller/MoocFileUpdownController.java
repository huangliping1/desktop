package com.ruoyi.project.mooc.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mooc.domain.MoocSerialNumber;
import com.ruoyi.project.mooc.service.IMoocSerialNumberService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author administrator
 * @date 2020-04-26
 */
@Controller
@RequestMapping("/mooc/fileud")
public class MoocFileUpdownController extends BaseController
{
    private String prefix = "mooc/fileupdown";

    @Autowired
    private IMoocSerialNumberService moocSerialNumberService;

    @RequiresPermissions("mooc:fileupdown:view")
    @GetMapping()
    public String number()
    {
        return prefix + "/up";
    }
    
}
