package com.ruoyi.project.mooc.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mooc.domain.MoocQuestion;
import com.ruoyi.project.mooc.service.IMoocQuestionService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author administrator
 * @date 2020-05-06
 */
@Controller
@RequestMapping("/mooc/question")
public class MoocQuestionController extends BaseController
{
    private String prefix = "mooc/question";

    @Autowired
    private IMoocQuestionService moocQuestionService;

    @RequiresPermissions("mooc:question:view")
    @GetMapping()
    public String question()
    {
        return prefix + "/question";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:question:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MoocQuestion moocQuestion)
    {
        startPage();
        List<MoocQuestion> list = moocQuestionService.selectMoocQuestionList(moocQuestion);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:question:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MoocQuestion moocQuestion)
    {
        List<MoocQuestion> list = moocQuestionService.selectMoocQuestionList(moocQuestion);
        ExcelUtil<MoocQuestion> util = new ExcelUtil<MoocQuestion>(MoocQuestion.class);
        return util.exportExcel(list, "question");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:question:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MoocQuestion moocQuestion)
    {
        return toAjax(moocQuestionService.insertMoocQuestion(moocQuestion));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{questionId}")
    public String edit(@PathVariable("questionId") String questionId, ModelMap mmap)
    {
        MoocQuestion moocQuestion = moocQuestionService.selectMoocQuestionById(questionId);
        mmap.put("moocQuestion", moocQuestion);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:question:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MoocQuestion moocQuestion)
    {
        return toAjax(moocQuestionService.updateMoocQuestion(moocQuestion));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("mooc:question:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(moocQuestionService.deleteMoocQuestionByIds(ids));
    }
}
