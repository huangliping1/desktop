package com.ruoyi.project.mooc.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mooc.domain.MoocUpdateSystem;
import com.ruoyi.project.mooc.service.IMoocUpdateSystemService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author administrator
 * @date 2020-04-26
 */
@Controller
@RequestMapping("/mooc/system")
public class MoocUpdateSystemController extends BaseController
{
    private String prefix = "mooc/system";

    @Autowired
    private IMoocUpdateSystemService moocUpdateSystemService;

    @RequiresPermissions("mooc:system:view")
    @GetMapping()
    public String system()
    {
        return prefix + "/system";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:system:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MoocUpdateSystem moocUpdateSystem)
    {
        startPage();
        List<MoocUpdateSystem> list = moocUpdateSystemService.selectMoocUpdateSystemList(moocUpdateSystem);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:system:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MoocUpdateSystem moocUpdateSystem)
    {
        List<MoocUpdateSystem> list = moocUpdateSystemService.selectMoocUpdateSystemList(moocUpdateSystem);
        ExcelUtil<MoocUpdateSystem> util = new ExcelUtil<MoocUpdateSystem>(MoocUpdateSystem.class);
        return util.exportExcel(list, "system");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:system:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MoocUpdateSystem moocUpdateSystem)
    {
        return toAjax(moocUpdateSystemService.insertMoocUpdateSystem(moocUpdateSystem));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{uuid}")
    public String edit(@PathVariable("uuid") String uuid, ModelMap mmap)
    {
        MoocUpdateSystem moocUpdateSystem = moocUpdateSystemService.selectMoocUpdateSystemById(uuid);
        mmap.put("moocUpdateSystem", moocUpdateSystem);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:system:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MoocUpdateSystem moocUpdateSystem)
    {
        return toAjax(moocUpdateSystemService.updateMoocUpdateSystem(moocUpdateSystem));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("mooc:system:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(moocUpdateSystemService.deleteMoocUpdateSystemByIds(ids));
    }
}
