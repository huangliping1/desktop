package com.ruoyi.project.mooc.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.common.CommonController;
import com.ruoyi.project.mooc.domain.MoocDirectlive;
import com.ruoyi.project.mooc.service.IMoocDirectliveService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author administrator
 * @date 2020-05-06
 */
@Controller
@RequestMapping("/mooc/directlive")
public class MoocDirectliveController extends BaseController
{
    private String prefix = "mooc/directlive";

    @Autowired
    private IMoocDirectliveService moocDirectliveService;

    @RequiresPermissions("mooc:directlive:view")
    @GetMapping()
    public String directlive()
    {
        return prefix + "/directlive";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:directlive:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MoocDirectlive moocDirectlive)
    {
        startPage();
        List<MoocDirectlive> list = moocDirectliveService.selectMoocDirectliveList(moocDirectlive);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:directlive:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MoocDirectlive moocDirectlive)
    {
        List<MoocDirectlive> list = moocDirectliveService.selectMoocDirectliveList(moocDirectlive);
        ExcelUtil<MoocDirectlive> util = new ExcelUtil<MoocDirectlive>(MoocDirectlive.class);
        return util.exportExcel(list, "directlive");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:directlive:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MoocDirectlive moocDirectlive)
    {
    	@SuppressWarnings("rawtypes")
		Map pmap=new HashMap();
       //Object pamp;
		//pamp.put();
		pmap.put("seralname", "videoseral");
       String ss=CommonController.getseralnumber(pmap);
       moocDirectlive.setVideoIdkey(ss);
        return toAjax(moocDirectliveService.insertMoocDirectlive(moocDirectlive));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{videoId}")
    public String edit(@PathVariable("videoId") Long videoId, ModelMap mmap)
    {
        MoocDirectlive moocDirectlive = moocDirectliveService.selectMoocDirectliveById(videoId);
        mmap.put("moocDirectlive", moocDirectlive);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:directlive:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MoocDirectlive moocDirectlive)
    {
        return toAjax(moocDirectliveService.updateMoocDirectlive(moocDirectlive));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("mooc:directlive:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(moocDirectliveService.deleteMoocDirectliveByIds(ids));
    }
}
