package com.ruoyi.project.mooc.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mooc.domain.MoocJoincourse;
import com.ruoyi.project.mooc.service.IMoocJoincourseService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author administrator
 * @date 2020-05-13
 */
@Controller
@RequestMapping("/mooc/joincourse")
public class MoocJoincourseController extends BaseController
{
    private String prefix = "mooc/joincourse";

    @Autowired
    private IMoocJoincourseService moocJoincourseService;

    @RequiresPermissions("mooc:joincourse:view")
    @GetMapping()
    public String joincourse()
    {
        return prefix + "/joincourse";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:joincourse:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MoocJoincourse moocJoincourse)
    {
        startPage();
        List<MoocJoincourse> list = moocJoincourseService.selectMoocJoincourseList(moocJoincourse);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:joincourse:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MoocJoincourse moocJoincourse)
    {
        List<MoocJoincourse> list = moocJoincourseService.selectMoocJoincourseList(moocJoincourse);
        ExcelUtil<MoocJoincourse> util = new ExcelUtil<MoocJoincourse>(MoocJoincourse.class);
        return util.exportExcel(list, "joincourse");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:joincourse:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MoocJoincourse moocJoincourse)
    {
        return toAjax(moocJoincourseService.insertMoocJoincourse(moocJoincourse));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{joinId}")
    public String edit(@PathVariable("joinId") String joinId, ModelMap mmap)
    {
        MoocJoincourse moocJoincourse = moocJoincourseService.selectMoocJoincourseById(joinId);
        mmap.put("moocJoincourse", moocJoincourse);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:joincourse:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MoocJoincourse moocJoincourse)
    {
        return toAjax(moocJoincourseService.updateMoocJoincourse(moocJoincourse));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("mooc:joincourse:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(moocJoincourseService.deleteMoocJoincourseByIds(ids));
    }
}
