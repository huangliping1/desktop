package com.ruoyi.project.mooc.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mooc.domain.MoocCourseContent;
import com.ruoyi.project.mooc.service.IMoocCourseContentService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author administrator
 * @date 2020-05-06
 */
@Controller
@RequestMapping("/mooc/content")
public class MoocCourseContentController extends BaseController
{
    private String prefix = "mooc/content";

    @Autowired
    private IMoocCourseContentService moocCourseContentService;

    @RequiresPermissions("mooc:content:view")
    @GetMapping()
    public String content()
    {
        return prefix + "/content";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:content:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MoocCourseContent moocCourseContent)
    {
        startPage();
        List<MoocCourseContent> list = moocCourseContentService.selectMoocCourseContentList(moocCourseContent);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:content:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MoocCourseContent moocCourseContent)
    {
        List<MoocCourseContent> list = moocCourseContentService.selectMoocCourseContentList(moocCourseContent);
        ExcelUtil<MoocCourseContent> util = new ExcelUtil<MoocCourseContent>(MoocCourseContent.class);
        return util.exportExcel(list, "content");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:content:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MoocCourseContent moocCourseContent)
    {
        return toAjax(moocCourseContentService.insertMoocCourseContent(moocCourseContent));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{typeId}")
    public String edit(@PathVariable("typeId") String typeId, ModelMap mmap)
    {
        MoocCourseContent moocCourseContent = moocCourseContentService.selectMoocCourseContentById(typeId);
        mmap.put("moocCourseContent", moocCourseContent);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:content:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MoocCourseContent moocCourseContent)
    {
        return toAjax(moocCourseContentService.updateMoocCourseContent(moocCourseContent));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("mooc:content:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(moocCourseContentService.deleteMoocCourseContentByIds(ids));
    }
}
