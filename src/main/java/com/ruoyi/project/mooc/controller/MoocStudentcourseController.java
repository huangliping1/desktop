package com.ruoyi.project.mooc.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mooc.domain.MoocStudentcourse;
import com.ruoyi.project.mooc.service.IMoocStudentcourseService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author administrator
 * @date 2020-05-06
 */
@Controller
@RequestMapping("/mooc/studentcourse")
public class MoocStudentcourseController extends BaseController
{
    private String prefix = "mooc/studentcourse";

    @Autowired
    private IMoocStudentcourseService moocStudentcourseService;

    @RequiresPermissions("mooc:studentcourse:view")
    @GetMapping()
    public String studentcourse()
    {
        return prefix + "/studentcourse";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:studentcourse:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MoocStudentcourse moocStudentcourse)
    {
        startPage();
        List<MoocStudentcourse> list = moocStudentcourseService.selectMoocStudentcourseList(moocStudentcourse);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:studentcourse:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MoocStudentcourse moocStudentcourse)
    {
        List<MoocStudentcourse> list = moocStudentcourseService.selectMoocStudentcourseList(moocStudentcourse);
        ExcelUtil<MoocStudentcourse> util = new ExcelUtil<MoocStudentcourse>(MoocStudentcourse.class);
        return util.exportExcel(list, "studentcourse");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:studentcourse:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MoocStudentcourse moocStudentcourse)
    {
        return toAjax(moocStudentcourseService.insertMoocStudentcourse(moocStudentcourse));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{coursetableId}")
    public String edit(@PathVariable("coursetableId") String coursetableId, ModelMap mmap)
    {
        MoocStudentcourse moocStudentcourse = moocStudentcourseService.selectMoocStudentcourseById(coursetableId);
        mmap.put("moocStudentcourse", moocStudentcourse);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:studentcourse:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MoocStudentcourse moocStudentcourse)
    {
        return toAjax(moocStudentcourseService.updateMoocStudentcourse(moocStudentcourse));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("mooc:studentcourse:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(moocStudentcourseService.deleteMoocStudentcourseByIds(ids));
    }
}
