package com.ruoyi.project.mooc.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mooc.domain.MoocPicword;
import com.ruoyi.project.mooc.service.IMoocPicwordService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author administrator
 * @date 2020-05-13
 */
@Controller
@RequestMapping("/mooc/picword")
public class MoocPicwordController extends BaseController
{
    private String prefix = "mooc/picword";

    @Autowired
    private IMoocPicwordService moocPicwordService;

    @RequiresPermissions("mooc:picword:view")
    @GetMapping()
    public String picword()
    {
        return prefix + "/picword";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:picword:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MoocPicword moocPicword)
    {
        startPage();
        List<MoocPicword> list = moocPicwordService.selectMoocPicwordList(moocPicword);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:picword:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MoocPicword moocPicword)
    {
        List<MoocPicword> list = moocPicwordService.selectMoocPicwordList(moocPicword);
        ExcelUtil<MoocPicword> util = new ExcelUtil<MoocPicword>(MoocPicword.class);
        return util.exportExcel(list, "picword");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:picword:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MoocPicword moocPicword)
    {
        return toAjax(moocPicwordService.insertMoocPicword(moocPicword));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{idName}")
    public String edit(@PathVariable("idName") String idName, ModelMap mmap)
    {
        MoocPicword moocPicword = moocPicwordService.selectMoocPicwordById(idName);
        mmap.put("moocPicword", moocPicword);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:picword:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MoocPicword moocPicword)
    {
        return toAjax(moocPicwordService.updateMoocPicword(moocPicword));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("mooc:picword:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(moocPicwordService.deleteMoocPicwordByIds(ids));
    }
}
