package com.ruoyi.project.mooc.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.common.CommonController;
import com.ruoyi.project.mooc.domain.MoocUpdatefile;
import com.ruoyi.project.mooc.service.IMoocUpdatefileService;
import com.ruoyi.project.mooc.service.MoocUpdatefileServiceImpl;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author administrator
 * @date 2020-05-13
 */
@Controller
@RequestMapping("/mooc/updatefile")
public class MoocUpdatefileController extends BaseController
{
    private String prefix = "mooc/updatefile";

    @Autowired
    private IMoocUpdatefileService moocUpdatefileService;

    @RequiresPermissions("mooc:updatefile:view")
    @GetMapping()
    public String updatefile()
    {
        return prefix + "/updatefile";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:updatefile:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MoocUpdatefile moocUpdatefile)
    {
  //  	moocUpdatefile=null;
        startPage();
        List<MoocUpdatefile> list = moocUpdatefileService.selectMoocUpdatefileList(moocUpdatefile);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:updatefile:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MoocUpdatefile moocUpdatefile)
    {
        List<MoocUpdatefile> list = moocUpdatefileService.selectMoocUpdatefileList(moocUpdatefile);
        ExcelUtil<MoocUpdatefile> util = new ExcelUtil<MoocUpdatefile>(MoocUpdatefile.class);
        return util.exportExcel(list, "updatefile");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:updatefile:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MoocUpdatefile moocUpdatefile)
    {
    	Map pmap=new HashMap();
		pmap.put("seralname", "fileseral");
	    String ss=CommonController.getseralnumber(pmap);
	    moocUpdatefile.setFileFerequency("1");;
	    moocUpdatefile.setFileLength("1");
		moocUpdatefile.setIdName(ss);
	    
        return toAjax(moocUpdatefileService.insertMoocUpdatefile(moocUpdatefile));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{idName}")
    public String edit(@PathVariable("idName") String idName, ModelMap mmap)
    {
        MoocUpdatefile moocUpdatefile = moocUpdatefileService.selectMoocUpdatefileById(idName);
        mmap.put("moocUpdatefile", moocUpdatefile);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:updatefile:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MoocUpdatefile moocUpdatefile)
    {
        return toAjax(moocUpdatefileService.updateMoocUpdatefile(moocUpdatefile));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("mooc:updatefile:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(moocUpdatefileService.deleteMoocUpdatefileByIds(ids));
    }
}
