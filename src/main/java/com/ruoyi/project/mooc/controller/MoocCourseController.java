package com.ruoyi.project.mooc.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mooc.domain.MoocCourse;
import com.ruoyi.project.mooc.service.IMoocCourseService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author administrator
 * @date 2020-05-06
 */
@Controller
@RequestMapping("/mooc/course")
public class MoocCourseController extends BaseController
{
    private String prefix = "mooc/course";

    @Autowired
    private IMoocCourseService moocCourseService;

    @RequiresPermissions("mooc:course:view")
    @GetMapping()
    public String course()
    {
        return prefix + "/course";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:course:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MoocCourse moocCourse)
    {
        startPage();
        List<MoocCourse> list = moocCourseService.selectMoocCourseList(moocCourse);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:course:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MoocCourse moocCourse)
    {
        List<MoocCourse> list = moocCourseService.selectMoocCourseList(moocCourse);
        ExcelUtil<MoocCourse> util = new ExcelUtil<MoocCourse>(MoocCourse.class);
        return util.exportExcel(list, "course");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:course:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MoocCourse moocCourse)
    {
        return toAjax(moocCourseService.insertMoocCourse(moocCourse));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{courseId}")
    public String edit(@PathVariable("courseId") String courseId, ModelMap mmap)
    {
        MoocCourse moocCourse = moocCourseService.selectMoocCourseById(courseId);
        mmap.put("moocCourse", moocCourse);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:course:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MoocCourse moocCourse)
    {
        return toAjax(moocCourseService.updateMoocCourse(moocCourse));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("mooc:course:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(moocCourseService.deleteMoocCourseByIds(ids));
    }
}
