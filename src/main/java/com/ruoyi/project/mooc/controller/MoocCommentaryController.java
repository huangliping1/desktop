package com.ruoyi.project.mooc.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.common.CommonController;
import com.ruoyi.project.mooc.domain.MoocCommentary;
import com.ruoyi.project.mooc.service.IMoocCommentaryService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author administrator
 * @date 2020-05-06
 */
@Controller
@RequestMapping("/mooc/commentary")
public class MoocCommentaryController extends BaseController
{
    private String prefix = "mooc/commentary";

    @Autowired
    private IMoocCommentaryService moocCommentaryService;

    @RequiresPermissions("mooc:commentary:view")
    @GetMapping()
    public String commentary()
    {
        return prefix + "/commentary";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:commentary:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MoocCommentary moocCommentary)
    {
        startPage();
        List<MoocCommentary> list = moocCommentaryService.selectMoocCommentaryList(moocCommentary);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:commentary:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MoocCommentary moocCommentary)
    {
        List<MoocCommentary> list = moocCommentaryService.selectMoocCommentaryList(moocCommentary);
        ExcelUtil<MoocCommentary> util = new ExcelUtil<MoocCommentary>(MoocCommentary.class);
        return util.exportExcel(list, "commentary");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:commentary:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MoocCommentary moocCommentary)
    {
		Map pmap=new HashMap();
		pmap.put("seralname", "commentary");
	    String ss=CommonController.getseralnumber(pmap);
	    moocCommentary.setCommentaryId(ss);
	       
        return toAjax(moocCommentaryService.insertMoocCommentary(moocCommentary));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{commentaryId}")
    public String edit(@PathVariable("commentaryId") String commentaryId, ModelMap mmap)
    {
        MoocCommentary moocCommentary = moocCommentaryService.selectMoocCommentaryById(commentaryId);
        mmap.put("moocCommentary", moocCommentary);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:commentary:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MoocCommentary moocCommentary)
    {
        return toAjax(moocCommentaryService.updateMoocCommentary(moocCommentary));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("mooc:commentary:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(moocCommentaryService.deleteMoocCommentaryByIds(ids));
    }
}
