package com.ruoyi.project.mooc.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.mooc.domain.MoocVideo;
import com.ruoyi.project.mooc.service.MoocVideoService;
import com.ruoyi.project.system.dict.domain.DictType;
import com.ruoyi.project.system.dict.service.IDictTypeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

/**
 * 数据字典信息
 * 
 */
@Api("视频信息管理")
@Controller
@RequestMapping("/mooc/videos")
public class MoocVideoController extends BaseController
{
    private String prefix = "mooc/type";

    
    
    @Autowired
    private MoocVideoService moocVideoService;

    @RequiresPermissions("mooc:videos:view")
    @GetMapping()
    public String video()
    {
    	return prefix + "/type";
    }


    /**
     * 查询视频信息列表
     */
    @ApiOperation("获取视频信息列表")
    @RequiresPermissions("mooc:videos:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MoocVideo moocVideo)
    {
        startPage();
        List<MoocVideo> list = moocVideoService.selectMoocVideoList(moocVideo);
        return getDataTable(list);
    }

   
    /**
     * 导出视频信息列表
     */
    @RequiresPermissions("mooc:videos:export")
    @Log(title = "视频信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MoocVideo moocVideo)
    {
        List<MoocVideo> list = moocVideoService.selectMoocVideoList(moocVideo);
        ExcelUtil<MoocVideo> util = new ExcelUtil<MoocVideo>(MoocVideo.class);
        return util.exportExcel(list, "video");
    }
    


    /**
     * 新增视频信息
     */

    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }
    
    /**
     * 新增保存字典类型
     */
    @ApiOperation("新增视频信息")
    @Log(title = "视频信息", businessType = BusinessType.EXPORT)
    @RequiresPermissions("mooc:videos:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MoocVideo moocVideo)
    {
        return toAjax(moocVideoService.insertMoocVideo(moocVideo));
    }
    /**
     * 修改视频信息
     */
    @Transactional
    @ApiOperation("修改视频信息")
    @ApiImplicitParam(name = "MoocVideo", value = "修改视频信息", dataType = "MoocVideo")
    @GetMapping("/edit/{videoId}")
    public String edit(@PathVariable("videoId") Long videoId, ModelMap mmap)
    {
        MoocVideo moocVideo = moocVideoService.selectMoocVideoById(videoId);
        mmap.put("moocVideo", moocVideo);
        return prefix + "/edit";
    }

    /**
     * 修改保存视频信息
     */
    @RequiresPermissions("mooc:videos:edit")
    @Log(title = "视频信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MoocVideo moocVideo)
    {
        return toAjax(moocVideoService.updateMoocVideo(moocVideo));
    }

    /**
     * 删除视频信息
     */
    @ApiOperation("删除视频信息")
    @RequiresPermissions("mooc:videos:remove")
    @Log(title = "视频信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(moocVideoService.deleteMoocVideoByIds(ids));
    }
}
