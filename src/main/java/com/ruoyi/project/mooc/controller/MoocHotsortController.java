package com.ruoyi.project.mooc.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mooc.domain.MoocHotsort;
import com.ruoyi.project.mooc.service.IMoocHotsortService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author administrator
 * @date 2020-05-06
 */
@Controller
@RequestMapping("/mooc/hotsort")
public class MoocHotsortController extends BaseController
{
    private String prefix = "mooc/hotsort";

    @Autowired
    private IMoocHotsortService moocHotsortService;

    @RequiresPermissions("mooc:hotsort:view")
    @GetMapping()
    public String hotsort()
    {
        return prefix + "/hotsort";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:hotsort:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MoocHotsort moocHotsort)
    {
        startPage();
        List<MoocHotsort> list = moocHotsortService.selectMoocHotsortList(moocHotsort);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:hotsort:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MoocHotsort moocHotsort)
    {
        List<MoocHotsort> list = moocHotsortService.selectMoocHotsortList(moocHotsort);
        ExcelUtil<MoocHotsort> util = new ExcelUtil<MoocHotsort>(MoocHotsort.class);
        return util.exportExcel(list, "hotsort");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:hotsort:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MoocHotsort moocHotsort)
    {
        return toAjax(moocHotsortService.insertMoocHotsort(moocHotsort));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{rankingId}")
    public String edit(@PathVariable("rankingId") String rankingId, ModelMap mmap)
    {
        MoocHotsort moocHotsort = moocHotsortService.selectMoocHotsortById(rankingId);
        mmap.put("moocHotsort", moocHotsort);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:hotsort:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MoocHotsort moocHotsort)
    {
        return toAjax(moocHotsortService.updateMoocHotsort(moocHotsort));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("mooc:hotsort:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(moocHotsortService.deleteMoocHotsortByIds(ids));
    }
}
