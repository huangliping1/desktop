package com.ruoyi.project.mooc.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mooc.domain.MoocSerialNumber;
import com.ruoyi.project.mooc.service.IMoocSerialNumberService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author administrator
 * @date 2020-04-26
 */
@Controller
@RequestMapping("/mooc/number")
public class MoocSerialNumberController extends BaseController
{
    private String prefix = "mooc/number";

    @Autowired
    private IMoocSerialNumberService moocSerialNumberService;

    @RequiresPermissions("mooc:number:view")
    @GetMapping()
    public String number()
    {
        return prefix + "/number";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:number:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MoocSerialNumber moocSerialNumber)
    {
        startPage();
        List<MoocSerialNumber> list = moocSerialNumberService.selectMoocSerialNumberList(moocSerialNumber);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:number:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MoocSerialNumber moocSerialNumber)
    {
        List<MoocSerialNumber> list = moocSerialNumberService.selectMoocSerialNumberList(moocSerialNumber);
        ExcelUtil<MoocSerialNumber> util = new ExcelUtil<MoocSerialNumber>(MoocSerialNumber.class);
        return util.exportExcel(list, "number");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:number:add")
    @Log(title = "新增流水号类别", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MoocSerialNumber moocSerialNumber)
    {
        return toAjax(moocSerialNumberService.insertMoocSerialNumber(moocSerialNumber));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{idName}")
    public String edit(@PathVariable("idName") String idName, ModelMap mmap)
    {
        MoocSerialNumber moocSerialNumber = moocSerialNumberService.selectMoocSerialNumberById(idName);
        mmap.put("moocSerialNumber", moocSerialNumber);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:number:edit")
    @Log(title = "修改流水号类别信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MoocSerialNumber moocSerialNumber)
    {
        return toAjax(moocSerialNumberService.updateMoocSerialNumber(moocSerialNumber));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("mooc:number:remove")
    @Log(title = "删除流水号类别信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(moocSerialNumberService.deleteMoocSerialNumberByIds(ids));
    }
}
