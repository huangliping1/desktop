package com.ruoyi.project.mooc.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.common.CommonController;
import com.ruoyi.project.mooc.domain.MoocAnswerquestionType;
import com.ruoyi.project.mooc.service.IMoocAnswerquestionTypeService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author administrator
 * @date 2020-05-06
 */
@Controller
@RequestMapping("/mooc/classifytype")
public class MoocAnswerquestionTypeController extends BaseController
{
    private String prefix = "mooc/classifytype";

    @Autowired
    private IMoocAnswerquestionTypeService moocAnswerquestionTypeService;

    @RequiresPermissions("mooc:type:view")
    @GetMapping()
    public String type()
    {
        return prefix + "/type";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:type:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MoocAnswerquestionType moocAnswerquestionType)
    {
        startPage();
        List<MoocAnswerquestionType> list = moocAnswerquestionTypeService.selectMoocAnswerquestionTypeList(moocAnswerquestionType);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:type:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MoocAnswerquestionType moocAnswerquestionType)
    {
        List<MoocAnswerquestionType> list = moocAnswerquestionTypeService.selectMoocAnswerquestionTypeList(moocAnswerquestionType);
        ExcelUtil<MoocAnswerquestionType> util = new ExcelUtil<MoocAnswerquestionType>(MoocAnswerquestionType.class);
        return util.exportExcel(list, "type");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:type:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MoocAnswerquestionType moocAnswerquestionType)
    {
    	Map pmap=new HashMap();
		pmap.put("seralname", "classify");
	    String ss=CommonController.getseralnumber(pmap);
	    moocAnswerquestionType.setClassifyId(ss);
        return toAjax(moocAnswerquestionTypeService.insertMoocAnswerquestionType(moocAnswerquestionType));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{classifyId}")
    public String edit(@PathVariable("classifyId") String classifyId, ModelMap mmap)
    {
        MoocAnswerquestionType moocAnswerquestionType = moocAnswerquestionTypeService.selectMoocAnswerquestionTypeById(classifyId);
        mmap.put("moocAnswerquestionType", moocAnswerquestionType);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:type:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MoocAnswerquestionType moocAnswerquestionType)
    {
        return toAjax(moocAnswerquestionTypeService.updateMoocAnswerquestionType(moocAnswerquestionType));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("mooc:type:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(moocAnswerquestionTypeService.deleteMoocAnswerquestionTypeByIds(ids));
    }
}
