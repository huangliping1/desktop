package com.ruoyi.project.mooc.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mooc.domain.MoocAnswer;
import com.ruoyi.project.mooc.service.IMoocAnswerService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author administrator
 * @date 2020-05-06
 */
@Controller
@RequestMapping("/mooc/answer")
public class MoocAnswerController extends BaseController
{
    private String prefix = "mooc/answer";

    @Autowired
    private IMoocAnswerService moocAnswerService;

    @RequiresPermissions("mooc:answer:view")
    @GetMapping()
    public String answer()
    {
        return prefix + "/answer";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:answer:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MoocAnswer moocAnswer)
    {
        startPage();
        List<MoocAnswer> list = moocAnswerService.selectMoocAnswerList(moocAnswer);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:answer:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MoocAnswer moocAnswer)
    {
        List<MoocAnswer> list = moocAnswerService.selectMoocAnswerList(moocAnswer);
        ExcelUtil<MoocAnswer> util = new ExcelUtil<MoocAnswer>(MoocAnswer.class);
        return util.exportExcel(list, "answer");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:answer:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MoocAnswer moocAnswer)
    {
        return toAjax(moocAnswerService.insertMoocAnswer(moocAnswer));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{answerId}")
    public String edit(@PathVariable("answerId") String answerId, ModelMap mmap)
    {
        MoocAnswer moocAnswer = moocAnswerService.selectMoocAnswerById(answerId);
        mmap.put("moocAnswer", moocAnswer);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:answer:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MoocAnswer moocAnswer)
    {
        return toAjax(moocAnswerService.updateMoocAnswer(moocAnswer));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("mooc:answer:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(moocAnswerService.deleteMoocAnswerByIds(ids));
    }
}
