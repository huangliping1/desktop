package com.ruoyi.project.mooc.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mooc.domain.MoocCourseList;
import com.ruoyi.project.mooc.service.IMoocCourseListService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author administrator
 * @date 2020-05-06
 */
@Controller
@RequestMapping("/mooc/list")
public class MoocCourseListController extends BaseController
{
    private String prefix = "mooc/list";

    @Autowired
    private IMoocCourseListService moocCourseListService;

    @RequiresPermissions("mooc:list:view")
    @GetMapping()
    public String list()
    {
        return prefix + "/list";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:list:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MoocCourseList moocCourseList)
    {
        startPage();
        List<MoocCourseList> list = moocCourseListService.selectMoocCourseListList(moocCourseList);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("mooc:list:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MoocCourseList moocCourseList)
    {
        List<MoocCourseList> list = moocCourseListService.selectMoocCourseListList(moocCourseList);
        ExcelUtil<MoocCourseList> util = new ExcelUtil<MoocCourseList>(MoocCourseList.class);
        return util.exportExcel(list, "list");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:list:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MoocCourseList moocCourseList)
    {
        return toAjax(moocCourseListService.insertMoocCourseList(moocCourseList));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{typeId}")
    public String edit(@PathVariable("typeId") String typeId, ModelMap mmap)
    {
        MoocCourseList moocCourseList = moocCourseListService.selectMoocCourseListById(typeId);
        mmap.put("moocCourseList", moocCourseList);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("mooc:list:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MoocCourseList moocCourseList)
    {
        return toAjax(moocCourseListService.updateMoocCourseList(moocCourseList));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("mooc:list:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(moocCourseListService.deleteMoocCourseListByIds(ids));
    }
}
