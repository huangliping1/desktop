package com.ruoyi.project.mooc.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;

import com.ruoyi.project.mooc.domain.MoocVideo;
import com.ruoyi.project.mooc.domain.MoocVideop;
import com.ruoyi.project.system.dict.domain.DictType;

/**
 * 字典表 数据层
 * 
 */
@Mapper
public interface MoocVideoMapper
{
	 /**
     * 查询视频信息
     * 
     * @param videoId 视频信息ID
     * @return 视频信息
     */
    public MoocVideo selectMoocVideoById(Long videoId);

    /**
     * 查询视频信息列表
     * 
     * @param moocVideo 视频信息
     * @return 视频信息集合
     */
    public List<MoocVideo> selectMoocVideoList(MoocVideo moocVideo);

    public List<MoocVideop> selectMoocVideoListp(MoocVideop moocVideop); 
    
    /**
     * 新增视频信息
     * 
     * @param moocVideo 视频信息
     * @return 结果
     */
    public int insertMoocVideo(MoocVideo moocVideo);

    /**
     * 修改视频信息
     * 
     * @param moocVideo 视频信息
     * @return 结果
     */
    public int updateMoocVideo(MoocVideo moocVideo);

    /**
     * 删除视频信息
     * 
     * @param videoId 视频信息ID
     * @return 结果
     */
    public int deleteMoocVideoById(Long videoId);

    /**
     * 批量删除视频信息
     * 
     * @param videoIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteMoocVideoByIds(String[] videoIds);
   
}
