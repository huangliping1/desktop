package com.ruoyi.project.mooc.mapper;

import java.util.List;
import com.ruoyi.project.mooc.domain.MoocHotsort;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author administrator
 * @date 2020-05-06
 */
public interface MoocHotsortMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param rankingId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public MoocHotsort selectMoocHotsortById(String rankingId);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocHotsort 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MoocHotsort> selectMoocHotsortList(MoocHotsort moocHotsort);

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocHotsort 【请填写功能名称】
     * @return 结果
     */
    public int insertMoocHotsort(MoocHotsort moocHotsort);

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocHotsort 【请填写功能名称】
     * @return 结果
     */
    public int updateMoocHotsort(MoocHotsort moocHotsort);

    /**
     * 删除【请填写功能名称】
     * 
     * @param rankingId 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteMoocHotsortById(String rankingId);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param rankingIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteMoocHotsortByIds(String[] rankingIds);
}
