package com.ruoyi.project.mooc.mapper;

import java.util.List;
import com.ruoyi.project.mooc.domain.MoocPicword;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author administrator
 * @date 2020-05-13
 */
public interface MoocPicwordMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param idName 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public MoocPicword selectMoocPicwordById(String idName);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocPicword 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MoocPicword> selectMoocPicwordList(MoocPicword moocPicword);

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocPicword 【请填写功能名称】
     * @return 结果
     */
    public int insertMoocPicword(MoocPicword moocPicword);

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocPicword 【请填写功能名称】
     * @return 结果
     */
    public int updateMoocPicword(MoocPicword moocPicword);

    /**
     * 删除【请填写功能名称】
     * 
     * @param idName 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteMoocPicwordById(String idName);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param idNames 需要删除的数据ID
     * @return 结果
     */
    public int deleteMoocPicwordByIds(String[] idNames);
}
