package com.ruoyi.project.mooc.mapper;

import java.util.List;
import com.ruoyi.project.mooc.domain.MoocJoincourse;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author administrator
 * @date 2020-05-13
 */
public interface MoocJoincourseMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param joinId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public MoocJoincourse selectMoocJoincourseById(String joinId);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocJoincourse 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MoocJoincourse> selectMoocJoincourseList(MoocJoincourse moocJoincourse);

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocJoincourse 【请填写功能名称】
     * @return 结果
     */
    public int insertMoocJoincourse(MoocJoincourse moocJoincourse);

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocJoincourse 【请填写功能名称】
     * @return 结果
     */
    public int updateMoocJoincourse(MoocJoincourse moocJoincourse);

    /**
     * 删除【请填写功能名称】
     * 
     * @param joinId 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteMoocJoincourseById(String joinId);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param joinIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteMoocJoincourseByIds(String[] joinIds);
}
