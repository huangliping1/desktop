package com.ruoyi.project.mooc.mapper;

import java.util.List;
import com.ruoyi.project.mooc.domain.MoocCommentary;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author administrator
 * @date 2020-05-06
 */
public interface MoocCommentaryMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param commentaryId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public MoocCommentary selectMoocCommentaryById(String commentaryId);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocCommentary 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MoocCommentary> selectMoocCommentaryList(MoocCommentary moocCommentary);

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocCommentary 【请填写功能名称】
     * @return 结果
     */
    public int insertMoocCommentary(MoocCommentary moocCommentary);

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocCommentary 【请填写功能名称】
     * @return 结果
     */
    public int updateMoocCommentary(MoocCommentary moocCommentary);

    /**
     * 删除【请填写功能名称】
     * 
     * @param commentaryId 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteMoocCommentaryById(String commentaryId);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param commentaryIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteMoocCommentaryByIds(String[] commentaryIds);
}
