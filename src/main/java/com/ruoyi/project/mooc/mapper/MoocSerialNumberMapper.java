package com.ruoyi.project.mooc.mapper;

import java.util.List;
import com.ruoyi.project.mooc.domain.MoocSerialNumber;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author administrator
 * @date 2020-04-26
 */
public interface MoocSerialNumberMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param idName 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public MoocSerialNumber selectMoocSerialNumberById(String idName);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocSerialNumber 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MoocSerialNumber> selectMoocSerialNumberList(MoocSerialNumber moocSerialNumber);

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocSerialNumber 【请填写功能名称】
     * @return 结果
     */
    public int insertMoocSerialNumber(MoocSerialNumber moocSerialNumber);

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocSerialNumber 【请填写功能名称】
     * @return 结果
     */
    public int updateMoocSerialNumber(MoocSerialNumber moocSerialNumber);

    /**
     * 删除【请填写功能名称】
     * 
     * @param idName 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteMoocSerialNumberById(String idName);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param idNames 需要删除的数据ID
     * @return 结果
     */
    public int deleteMoocSerialNumberByIds(String[] idNames);
}
