package com.ruoyi.project.mooc.mapper;

import java.util.List;
import com.ruoyi.project.mooc.domain.MoocCourseContent;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author administrator
 * @date 2020-05-06
 */
public interface MoocCourseContentMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param typeId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public MoocCourseContent selectMoocCourseContentById(String typeId);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param moocCourseContent 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MoocCourseContent> selectMoocCourseContentList(MoocCourseContent moocCourseContent);

    /**
     * 新增【请填写功能名称】
     * 
     * @param moocCourseContent 【请填写功能名称】
     * @return 结果
     */
    public int insertMoocCourseContent(MoocCourseContent moocCourseContent);

    /**
     * 修改【请填写功能名称】
     * 
     * @param moocCourseContent 【请填写功能名称】
     * @return 结果
     */
    public int updateMoocCourseContent(MoocCourseContent moocCourseContent);

    /**
     * 删除【请填写功能名称】
     * 
     * @param typeId 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteMoocCourseContentById(String typeId);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param typeIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteMoocCourseContentByIds(String[] typeIds);
}
