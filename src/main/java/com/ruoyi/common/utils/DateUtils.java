package com.ruoyi.common.utils;

import java.lang.management.ManagementFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.project.monitor.logininfor.service.LogininforServiceImpl;
import com.ruoyi.project.mooc.mapper.MoocSerialNumberMapper;
import com.ruoyi.project.mooc.service.IMoocSerialNumberService;
import com.ruoyi.project.mooc.service.MoocSerialNumberServiceImpl;


/**
 * 时间工具类
 * 
 * @author ruoyi
 */
@Component 
public abstract class DateUtils extends org.apache.commons.lang3.time.DateUtils  implements ApplicationContextAware
{
	

  //  @Resource(name = "moocSerialNumberService")
    
    
    public static String YYYY = "yyyy";

    public static String YYYY_MM = "yyyy-MM";

    public static String YYYY_MM_DD = "yyyy-MM-dd";

    public static String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";

    public static String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    private static String[] parsePatterns = {
            "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM", 
            "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM",
            "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm", "yyyy.MM"};
    
    private static int counter = 0;

    /**
     * 获取当前Date型日期
     * 
     * @return Date() 当前日期
     */
    public static Date getNowDate()
    {
        return new Date();
    }

    /**
     * 获取当前日期, 默认格式为yyyy-MM-dd
     * 
     * @return String
     */
    public static String getDate()
    {
        return dateTimeNow(YYYY_MM_DD);
    }

    public static final String getTime()
    {
        return dateTimeNow(YYYY_MM_DD_HH_MM_SS);
    }

    public static final String dateTimeNow()
    {
        return dateTimeNow(YYYYMMDDHHMMSS);
    }

    public static final String dateTimeNow(final String format)
    {
        return parseDateToStr(format, new Date());
    }

    public static final String dateTime(final Date date)
    {
        return parseDateToStr(YYYY_MM_DD, date);
    }

    public static final String parseDateToStr(final String format, final Date date)
    {
        return new SimpleDateFormat(format).format(date);
    }

    public static final Date dateTime(final String format, final String ts)
    {
        try
        {
            return new SimpleDateFormat(format).parse(ts);
        }
        catch (ParseException e)
        {
            throw new RuntimeException(e);
        }
    }

    /**
     * 日期路径 即年/月/日 如2018/08/08
     */
    public static final String datePath()
    {
        Date now = new Date();
        return DateFormatUtils.format(now, "yyyy/MM/dd");
    }
    /**
     * 日期路径 即年/月/日 如2018/08/08
     */
    public static final String datePathyear()
    {
        Date now = new Date();
        return DateFormatUtils.format(now, "yyyy");
    }
    /**
     * 日期路径 即年/月/日 如20180808
     */
    public static final String dateTime()
    {
        Date now = new Date();
        return DateFormatUtils.format(now, "yyyyMMdd");
    }

    /**
     * 日期型字符串转化为日期 格式
     */
    public static Date parseDate(Object str)
    {
        if (str == null)
        {
            return null;
        }
        try
        {
            return parseDate(str.toString(), parsePatterns);
        }
        catch (ParseException e)
        {
            return null;
        }
    }

    /**
     * 获取服务器启动时间
     */
    public static Date getServerStartDate()
    {
        long time = ManagementFactory.getRuntimeMXBean().getStartTime();
        return new Date(time);
    }

    /**
     * 计算两个时间差
     */
    public static String getDatePoor(Date endDate, Date nowDate)
    {
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - nowDate.getTime();
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;
        return day + "天" + hour + "小时" + min + "分钟";
    }
    
        
    /**
     * 编码主键
     */
    @SuppressWarnings("unused")
	public static final String encodingkeyid(String key)
    {
      //  fileName = fileName.replace("_", " ");
    	String id = UUID.randomUUID().toString(); 
        key = Md5Utils.hash(key + System.nanoTime() + counter++);
        return key;
    }
    
    
    /**
     * 获取流水
     */
    
	@Autowired
    private static IMoocSerialNumberService moocSerialNumberService;
    
    
	@Autowired
    private static MoocSerialNumberMapper moocSerialNumberMapper;
    
    public static DateUtils testUtils;
    
    
    @PostConstruct
    public void init() {    
        testUtils = this;
        testUtils.moocSerialNumberMapper = this.moocSerialNumberMapper;
     //   testUtils.moocSerialNumberService = this.moocSerialNumberService;
    } 
    
    //utils工具类中使用service和mapper接口的方法例子，用"testUtils.xxx.方法" 就可以了      
    @SuppressWarnings("static-access")
	public static  void test(String record){
    	
    	SpringUtils.getBean(MoocSerialNumberServiceImpl.class).selectMoocSerialNumberById(record);
    	//testUtils.moocSerialNumberService.selectMoocSerialNumberById(record);
    	
    }
    @SuppressWarnings("unused")
	@Autowired
	public static String getnumber(String key)
    {

  //  	testUtils.moocSerialNumberMapper.selectMoocSerialNumberById("1");
        key = Md5Utils.hash(key + System.nanoTime() + counter++);
        return key;
    }
       

}
